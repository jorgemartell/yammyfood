import * as React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import HomeStack from '../Stacks/HomeStack';
import AccountStack from '../Stacks/AccountStack';
import NotificationStack from '../Stacks/NotificationStack';
import ActivityHistoryStack from '../Stacks/ActivityHistoryStack';
import Documentation from '@src/components/screens/Documentation';
import TrackOrder from '@src/components/screens/TrackOrder/Router';
import OrderHistory from '@src/components/screens/OrderHistory';
import Api from '@src/Api'
import AuthContext from '@src/context/auth-context';
import AuthenticationStack from '@src/components/router/Stacks/AuthenticationStack';
import Parse from "parse/react-native.js";
import { AsyncStorage } from 'react-native';

type TabNavigationProps = {};
type TabBarIconProps = {
  focused: boolean;
  color: string;
  size: number;
};
const Tab = createBottomTabNavigator();
const {Navigator} = Tab;

const renderTabBarIcon = (routeName: string) => {
  return (props: TabBarIconProps) => {
    const {color} = props;
    let iconName = 'home';
    switch (routeName) {
      case 'Explore':
        iconName = 'home';
        break;
      case 'Entrega':
        iconName = 'map';
        break;
      case 'Notifications':
        iconName = 'bell';
        break;
      case 'Perfil':
        iconName = 'user';
        break;
      case 'Iniciar':
        iconName = 'user';
        break;
      case 'Documentation':
        iconName = 'book';
        break;
      default:
        break;
    }
    return <Icon name={iconName} solid size={24} color={color} />;
  };
};

const TabNavigation: React.FC<TabNavigationProps> = () => {

    const [state, setState] = React.useState({show:false,data:false});
    const {userToken} = React.useContext(AuthContext);

    React.useEffect(() => {

        

        AsyncStorage.getItem('objectId').then(async(UserId)=>{

        let query = new Parse.Query('ordenes');


        query.equalTo("usuario", {"__type": "Pointer","className": "users",objectId:UserId});
        let subscription = await query.subscribe();
          
        subscription.on('update', (object) => {
          
            Api.OrderUser((data)=>{

              if(data.length!=0){
                setState({...state,show:true})
              }else{
                setState({...state,show:false})
              }
            })
        })

        subscription.on('create', (object) => {
          
            Api.OrderUser((data)=>{

              if(data.length!=0){
                setState({...state,show:true})
              }else{
                setState({...state,show:false})
              }
            })
        })
      })

      Api.OrderUser((data)=>{

        if(data.length!=0){
          setState({...state,show:true})
        }else{
          setState({...state,show:false})
        }
      })
    },[])

  var t_en = ()=>{

    if(state.show && userToken!=''){
      return (<Tab.Screen name="Entrega" component={TrackOrder} />)
    }else{
      return (<></>)
    }
  }
  Api.ShowEntrega = (en)=>{
    
    setState({...state,show:en})
  }

  return (
    <Navigator
      initialRouteName="Home"
      screenOptions={(props) => {
        const {
          route: {name: routeName},
        } = props;
        return {
          tabBarIcon: renderTabBarIcon(routeName),
        };
      }}>
      <Tab.Screen name="Restaurantes" component={HomeStack} />
      {t_en()}
      
      {userToken !='' &&
        <Tab.Screen name="Perfil" component={AccountStack} />
      }

      {userToken =='' &&
        <Tab.Screen name="Iniciar" component={AccountStack} />
      }
      
    </Navigator>
  );
};

export default TabNavigation;
