/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import 'react-native-gesture-handler';
import React, {useContext} from 'react';
import {StatusBar, View, Platform} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import ThemeContext from '@src/context/theme-context';
import TabNavigation from '@src/components/routes/TabNavigation';
import DishDetails from '@src/components/screens/DishDetails';
import SubProductos from '@src/components/screens/SubProductos';
import AuthenticationStack from '@src/components/routes/Stacks/AuthenticationStack';
import {lightTheme, darkTheme} from '@src/styles/theme';
import AuthContext from '@src/context/auth-context';
import auth from '@react-native-firebase/auth';
import { AsyncStorage } from 'react-native';
import Api from '@src/Api'

const RootStack = createStackNavigator();

const RootNavigation = () => {
  const {theme} = useContext(ThemeContext);
  const {userToken,signIn} = useContext(AuthContext);
  const flex = 1;
  const rootContainerBackgroundColor =lightTheme.colors.background
  const screenOptions =
    Platform.OS === 'ios'
      ? {
          ...TransitionPresets.ModalSlideFromBottomIOS,
        }
      : {
          ...TransitionPresets.FadeFromBottomAndroid,
        };




    React.useEffect(() => {



      if(userToken!=null){
        Api.GetUserId((er)=>{
          if(er!=null){
            Api.GetUser((data,id,ref)=>{

              signIn();


            })
          }
        })
      }
     
    return ()=>{
      
    }; // unsubscribe on unmount
  }, []);




  return (
    <NavigationContainer theme={lightTheme }>
      <View style={{flex, backgroundColor: rootContainerBackgroundColor}}>
        <StatusBar
          backgroundColor={lightTheme.colors.background}
          barStyle={theme === 'light' ? 'dark-content' : 'light-content'}
        />
        <RootStack.Navigator mode="modal" screenOptions={screenOptions}>
          
            <RootStack.Screen
              name="Main"
              options={{headerShown: false}}
              component={TabNavigation}
            />
          
            <RootStack.Screen
              options={{
                headerTransparent: true,
                title: '',
                headerShown: false,
              }}
              name="Auth"
              component={AuthenticationStack}
            />
          
          <RootStack.Screen
            options={{
              headerTransparent: true,
              title: '',
              headerBackTitleVisible: false,
            }}
            name="DishDetailsModal"
            component={DishDetails}
          />
          
          <RootStack.Screen
            options={{
              
              title: 'Platillos',
             
            }}
            name="SubProductos"
            component={SubProductos}
          />

        </RootStack.Navigator>
      </View>
    </NavigationContainer>
  );
};

export default RootNavigation;
