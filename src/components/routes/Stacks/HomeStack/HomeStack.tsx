import * as React from 'react';
import {View,PermissionsAndroid} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {Icon, Text,Button} from '@src/components/elements';
import Home from '@src/components/screens/Home';
import PlaceDetails from '@src/components/screens/PlaceDetails';
import PlaceList from '@src/components/screens/PlaceList';
import Checkout from '@src/components/routes/Stacks/CheckoutStack';
import styles from './styles';
import {ScreenNavigationProps} from '../types';
import Geolocation from 'react-native-geolocation-service';
import Api from '@src/Api'
import {SearchBar, LoadingIndicator} from '@src/components/elements';
import DriverNotas from '@src/components/screens/DriverNotas'
import CartContext from '@src/context/cart-context';

type HomeStackProps = {} & ScreenNavigationProps;
type HomeStackParamList = {
  HomeScreen: undefined;
  PlaceDetailsScreen: undefined;
  CheckoutScreen: undefined;
  PlaceListScreen: {
    title?: string;
  };
};
const Stack = createStackNavigator<HomeStackParamList>();

const HomeStack: React.FC<HomeStackProps> = ({navigation}) => {

  Api.ToEngtrga = ()=>{ navigation.navigate('Entrega') }

  const {SetAddres,Addres} = React.useContext(CartContext);






    
  React.useEffect(() => {


    

    
  }, []);



  const _renderExploreHeaderTitle = () => {
    return (
      <View style={styles.headerLeftContainer}>
        <Icon
          name="map-marker-alt"
          size={18}
          style={styles.locationIcon}
          isPrimary
        />
        <Text style={styles.headerTitle}>{Addres.address}</Text>
      </View>
    );
  };

  const _renderExploreHeaderRight = () => {
    return (
      <Icon
        name="bell"
        size={18}
        isPrimary
        solid
        onPress={() => navigation.navigate('Notifications')}
      />
    );
  };
 
  return (
    <Stack.Navigator initialRouteName="HomeScreen">
      <Stack.Screen
        options={() => {
          return {
            headerTitle: _renderExploreHeaderTitle,
            title: 'Atras',
            headerTitleAlign: 'left',
            headerRight: _renderExploreHeaderRight,
            headerRightContainerStyle: styles.headerRightContainer,
          };
        }}
        name="HomeScreen"
        component={Home}
      />
      <Stack.Screen
        options={(de) => {

          return {
            headerTitle: de.route.params.doc.nombre,
          };
        }}
        name="PlaceDetailsScreen"
        component={PlaceDetails}
      />
      <Stack.Screen
        options={({route: {params}}) => {
          return {
            headerTitle: params?.title || 'Places',
          };
        }}
        name="PlaceListScreen"
        component={PlaceList}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="CheckoutScreen"
        component={Checkout}
      />
      <Stack.Screen
        
        name="DriverNotas"
        component={DriverNotas}
      />
    </Stack.Navigator>
  );
};

export default HomeStack;
