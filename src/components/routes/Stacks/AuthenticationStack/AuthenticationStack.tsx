import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Authentication from '@src/components/screens/Authentication';
import AuthWithPhoneNumber from '@src/components/screens/AuthWithPhoneNumber';
import AuthVerificationCode from '@src/components/screens/AuthVerificationCode';
import Login from '@src/components/screens/Login';
import ForgotPassword from '@src/components/screens/ForgotPassword';
import AuthWithEmail from '@src/components/screens/AuthWithEmail';
import SingUpWithEmail from '@src/components/screens/SingUpWithEmail';

import SendPasswordResetEmail from '@src/components/screens/SendPasswordResetEmail';

type AuthenticationStackProps = {};
const Stack = createStackNavigator();

const AuthenticationStack: React.FC<AuthenticationStackProps> = (param) => {

  return (
    <Stack.Navigator
      initialRouteName="AuthWithPhoneNumberScreen"
      screenOptions={{
        title: '',
        headerTransparent: true,
      }}>
      <Stack.Screen name="AuthenticationScreen" initialParams={{ login: param.route.params.login }} component={Authentication} />
      <Stack.Screen
        options={{
          headerBackTitle: 'Atras',
          headerTransparent: true,
        }}
        name="AuthWithPhoneNumberScreen"
        component={AuthWithPhoneNumber}
      />

      <Stack.Screen
        options={{
          
        }}
        name="AuthWithEmail"
        component={AuthWithEmail}
      />

<Stack.Screen
        options={{
          
        }}
        name="SingUpWithEmail"
        component={SingUpWithEmail}
      />

<Stack.Screen
        options={{
          
        }}
        name="SendPasswordResetEmail"
        component={SendPasswordResetEmail}
      />
      <Stack.Screen
        name="AuthVerificationCodeScreen"
        component={AuthVerificationCode}
      />
      <Stack.Screen name="LoginScreen" component={Login} />
      <Stack.Screen name="ForgotPasswordScreen" component={ForgotPassword} />
    </Stack.Navigator>
  );
};

export default AuthenticationStack;
