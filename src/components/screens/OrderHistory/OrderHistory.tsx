import * as React from 'react';
import {View} from 'react-native';
import styles from './styles';
import {List, Button, Text} from '@src/components/elements';
import {orderHistoryList} from '@src/data/mock-order-history';
import {ListRowItemProps} from '@src/components/elements/List/ListRowItem';
import Api from '@src/Api'
import Restaurante from './Restaurante'
type OrderHistoryProps = {};

const OrderHistory: React.FC<OrderHistoryProps> = () => {



  const [data, setData] = React.useState([]);


   React.useEffect(() => {
     
      Api.Where('ordenes',[],(d)=>{
        
         
        const list: ListRowItemProps[] = d.map((item) => {
        const {id, date, name, total,items} = item;

        return {
            id,
            title: <Restaurante id={item.res_id} />,
            subTitle: `${items.length} articulos | $${total}`,
            note: date,
            rightContainerStyle: styles.rightItemContainerStyle,
            rightIcon: (<></>),
          };
        });


        setData(list)

      })
    },[])



  
  return (
    <View style={styles.root}>
      <List data={data} />
    </View>
  );
};

export default OrderHistory;
