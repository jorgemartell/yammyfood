import * as React from 'react';
import {View} from 'react-native';
import styles from './styles';
import {List, Button, Text} from '@src/components/elements';
import {orderHistoryList} from '@src/data/mock-order-history';
import {ListRowItemProps} from '@src/components/elements/List/ListRowItem';
import Api from '@src/Api'

type OrderHistoryProps = {
};

const OrderHistory: React.FC<OrderHistoryProps> = ({id}) => {


  const [data, setData] = React.useState('');


   React.useEffect(() => {
      Api.Get('empresas',id,(d)=>{
        
      
        setData(d.nombre)

      })
    },[data])



  
  return (
    <View style={styles.root}>
      <Text isBold isPrimary>
                 {data}
                </Text>
    </View>
  );
};

export default OrderHistory;
