import * as React from 'react';
import {View,InteractionManager,SafeAreaView,} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {Container, Text, CheckBox} from '@src/components/elements';
import {Dish} from '@src/data/mock-places';
import styles from './styles';
import Api from '@src/Api'
import { useNavigation,useFocusEffect} from '@react-navigation/native';
import { LoadingIndicator} from '@src/components/elements';
import Item from './Item';


const SideDishes: React.FC = ({route}) => {

    const {
    colors: {border},
  } = useTheme();
  const navigation = useNavigation();
  const [data_list,set_data_list] = React.useState([]);

  useFocusEffect(()=>{
    Api.List('subproductos',(data)=>{

        set_data_list(data)

    })
  })


  var Prod = (dish,dishIndex)=>{
    return (<Container
              key={dishIndex}
              style={[styles.dishItemContainer, {borderBottomColor: border}]}>
              <Container style={styles.checkBoxContainer}>
                  <Text onPress={()=>{ route.params.end(dish); navigation.goBack() }} style={styles.sectionTitle}>{dish.nombre}</Text>
              
              </Container>
            </Container>)
  }
  

  var er = []

  for (var i = 0; i < data_list.length; i++) {
    
    if(route.params.sb_prod().includes(data_list[i].objectId)==false){
      er.push(data_list[i])
    }
  }
  return (
    <SafeAreaView>
      
        <View>
          
          {er.map((dish, dishIndex) => (
            <>{Prod(dish,dishIndex)}</>
          ))}
        </View>
      
   </SafeAreaView>
  );
};

export default SideDishes;
