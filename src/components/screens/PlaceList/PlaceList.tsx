import * as React from 'react';
import {View,InteractionManager} from 'react-native';
import {List} from '@src/components/elements';
import {mockPlaceList} from '@src/data/mock-places';
import PlaceListItem from '@src/components/common/PlaceListItem';
import styles from './styles';
import Api from '@src/Api'
import database from '@react-native-firebase/database';
import DishItem from '@src/components/common/DishItem';
import { useNavigation,useFocusEffect} from '@react-navigation/native';
import { LoadingIndicator} from '@src/components/elements';


type PlaceListProps = {};

const PlaceList: React.FC<PlaceListProps> = (data) => {

	var id = data.route.params.objectId
	const [get_list,set_list] = React.useState([]);

	const navigation = useNavigation();

	useFocusEffect(
    	React.useCallback(() => {
      const task = InteractionManager.runAfterInteractions(() => {

      		var l = []

      		var Empresa = (res_id,er,i)=>{
      			
      			Api.Get('empresas',res_id,(res_doc)=>{
		            	
		            		
		            	l.push(<DishItem OnAddGoTo={()=>{ navigation.navigate('PlaceDetailsScreen',{id:res_id,doc:res_doc});  }} data={er} res={res_doc} />)
		            	
		            	if(l.length==i){
		            		set_list(l)
		            	}

		            })

      		}
			Api.Where('productos',[['categoria','==',Api.GetDoc('categorias',id)]],(docs)=>{
		        

		        
		        
				for (var i = 0; i < docs.length; i++) {
					
					var img_url = "https://firebasestorage.googleapis.com/v0/b/delivery-23f4e.appspot.com/o/"+docs[i].img+"?alt=media"
		            var er = {
		                      doc:docs[i],
		                      objectId: docs[i].objectId,
		                      title: docs[i].nombre,
		                      description: docs[i].descripcion,
		                      price: docs[i].precio,
		                      image: {uri:img_url},
		                    }
		            const res_id = docs[i].empresa.objectId
		            Empresa(res_id,er,docs.length);
					
				}
				
		    })

		   	})

      return () => task.cancel();
		},[])
	)

	if(get_list.length==0){
		return (<LoadingIndicator size="large" hasMargin />)
	}
  return (
    <View style={styles.root}>
      {get_list}
    </View>
  );
};

export default PlaceList;
