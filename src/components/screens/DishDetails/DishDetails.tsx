import * as React from 'react';
import {
  Animated,
  SafeAreaView,
  View,
  KeyboardAvoidingView,
  Platform,
  InteractionManager
} from 'react-native';
import {useTheme, useNavigation} from '@react-navigation/native';
import {Text, Button} from '@src/components/elements';
import {mockDishDetails, Dish} from '@src/data/mock-places';
import CartContext from '@src/context/cart-context';
import HeadingInformation from './HeadingInformation';
import SideDishes from './SideDishes';
import SubProductos from './SubProductos';
import AddToBasketForm from './AddToBasketForm';
import {formatCurrency} from '@src/utils/number-formatter';
import styles from './styles';
import { LoadingIndicator} from '@src/components/elements';
import { useFocusEffect} from '@react-navigation/native';
import Api from '@src/Api'
type DishDetailsProps = {};

var detalles:Dish;

export const DishDetails: React.FC<DishDetailsProps> = (data) => {
  var doc = data.route.params.data.doc;
  var res_id = data.route.params.res.objectId;

  const [load_d,set_load_d] = React.useState(true);
  const [sub_p,set_sub_p] = React.useState(doc.sub_productos);
  const [qty, setqty] = React.useState(1);
  const [sub_cambios, set_sub_cambios] = React.useState(0);
  const [Comicion,setComicion] = React.useState(0);
  //var detalles = {title:doc.nombre,price:doc.precio,description:''}

  
  if(detalles==null){
    detalles = { latitude:doc.latitude,longitude:doc.longitude,objectId:doc.objectId,title:doc.nombre,price:doc.precio,description:doc.descripcion,sideDishes:[]}
  }else{
      if(doc.objectId!=detalles.objectId){
       detalles = { latitude:doc.latitude,longitude:doc.longitude,objectId:doc.objectId,title:doc.nombre,price:doc.precio,description:doc.descripcion,sideDishes:[]}
      }
  }

   var def = 1;

  if(doc.minimo!=undefined){
    def = parseInt(doc.minimo)
  }
  const [totalPrice, setTotalPrice] = React.useState({ extras:0,qty:def,total:(def*parseFloat(detalles.price)),base:parseFloat(detalles.price) });
  

  useFocusEffect(
    React.useCallback(() => {
     
      const task = InteractionManager.runAfterInteractions(() => {
        
        Api.Extras=0
        
         
            Api.Parametros('2002',(param)=>{
              
              setComicion(parseFloat(param))
            })

         

         });
      return () => task.cancel();
    }, []),
  );


if(doc.sub_productos.length != 0){

  Api.GetMulti('subproductos',doc.sub_productos,(docs)=>{
          var total = 0;
          
          for (var i = 0; i < docs.length; i++) {
            total += parseFloat(docs[i].precio)
          }
         
         })

}


  useFocusEffect(
    React.useCallback(() => {
      Api.Extras =0;
      const task = InteractionManager.runAfterInteractions(() => {
        

        if(doc.extras!=undefined){
        Api.List('extras',(docs)=>{
           
           var data = []
            for (var e = 0; e < doc.extras.length; ++e) {
              for (var d = 0; d < docs.length; ++d) {
                data.push(docs[d])
              }
            }
             

        Api.List('tipoextras',(tipodocs)=>{
            var cat = []


            for (var e = 0; e < doc.extras.length; ++e) {
              for (var d = 0; d < docs.length; ++d) {
                if(doc.extras[e]==docs[d].objectId){
                    var index = -1;
                    for (var i = 0; i < tipodocs.length; ++i) {
                        
                        for (var c = 0; c < cat.length; ++c) {
                            if(cat[c].objectId==tipodocs[i].objectId){
                                index = c;
                            }
                        }
                        if(index==-1){
                            cat.push({objectId:tipodocs[i].objectId,title:tipodocs[i].nombre,data:[]});
                            index = (cat.length-1)
                        }
                    }


                    cat[index].data.push({title:docs[d].nombre,price:docs[d].precio});


                }
              }
            }

            detalles.sideDishes=cat

            set_load_d(false)
          
        })

             
         
          })
      }else{

        set_load_d(false)
      }
         });
      return () => task.cancel();
    }, []),
  );

 


  const [selectedSideDishes, setSelectedSideDishes] = React.useState<Dish[]>(
    [],
  );
  const [scrollY] = React.useState(new Animated.Value(0));
  const {
    colors: {background},
  } = useTheme();
  const {goBack} = useNavigation();
  const {updateCartItems} = React.useContext(CartContext);



  const addSideDishToBasket = React.useCallback((dish: Dish,chenck) => {
      const existedDishIndex = selectedSideDishes.find((item: Dish) => item.objectId === dish.objectId );
      
     

      if (existedDishIndex) {
        setSelectedSideDishes(
          selectedSideDishes.filter((item: Dish) => item.objectId !== dish.objectId),
        );
        setTotalPrice({...totalPrice,total:totalPrice.total - parseFloat(existedDishIndex.price),extras:totalPrice.extras-parseFloat(existedDishIndex.price)});
      } else {
        setSelectedSideDishes([...selectedSideDishes, dish]);
        setTotalPrice({...totalPrice,total:totalPrice.total + parseFloat(dish.price),extras:totalPrice.extras+parseFloat(dish.price)});
      }



    },
    [selectedSideDishes, totalPrice.total,totalPrice.base,totalPrice.extras],
  );

  const updateTotalDishAmount = React.useCallback(
    (amount: number) => {
      const totalSelectedDishPrice = selectedSideDishes.reduce(
        (prevValue, currentValue) => prevValue + parseFloat(totalPrice.total),
        0,
      );

      setTotalPrice(
        {...totalPrice,total: (totalPrice.base * amount) + totalPrice.extras }
      );

    },
    [selectedSideDishes,totalPrice.total,totalPrice.extras,totalPrice.base],
  );


  const updateSubProd = React.useCallback(
    (amount) => {
      
      
      setTotalPrice({...totalPrice,base:amount,total:(amount*totalPrice.qty)+totalPrice.extras })


    },
    [selectedSideDishes,totalPrice.total,totalPrice.extras],
  );


  const onAddToBasketButtonPressed = () => {
     
    if('limit_extras' in doc){

      if(parseInt(doc.limit_extras)>0){
          if(Api.Extras!=parseInt(doc.limit_extras)){
            alert('Indique '+doc.limit_extras+" porciones")
            return
          }
      }
      

    }
    updateCartItems(
      [
        {
          dish: detalles,
          qty:qty,
          priceunit:(parseFloat(totalPrice.base)+(parseFloat(totalPrice.base)*Comicion/100))+ (sub_cambios*10),
          subtotal: (parseFloat(totalPrice.total)+(parseFloat(totalPrice.base)*Comicion/100))+ (sub_cambios*10),
          res:res_id,
          sideDishes: selectedSideDishes,
          sub_p:sub_p
        },
      ],
      totalPrice.total,
    );
    if(data.route.params.OnAddGoTo!=undefined){
      data.route.params.OnAddGoTo();
    }else{
        goBack();
    }
    
  };

  const coverTranslateY = scrollY.interpolate({
    inputRange: [-4, 0, 10],
    outputRange: [-2, 0, 3],
  });

  const coverScale = scrollY.interpolate({
    inputRange: [-200, 0],
    outputRange: [2, 1],
    extrapolateRight: 'clamp',
  });

  const headerOpacity = scrollY.interpolate({
    inputRange: [150, 250],
    outputRange: [0, 1],
    extrapolate: 'clamp',
  });
  var img_url = "https://firebasestorage.googleapis.com/v0/b/delivery-23f4e.appspot.com/o/"+doc.img+"?alt=media"


  if(load_d){
    return (<LoadingIndicator size="large" hasMargin />)
  }
  
  var prod = ()=>{

    return sub_p
  }
  var Reset = ()=>{
    if(sub_cambios!=0){
      return (<Text>Limpiar modificaciones</Text>)
    }
  }
  var change_su_prod = (_old,_new)=>{ 
    var _n = []

    if(sub_cambios==2){
      alert('Máximo 2 cambios')
      return
    }else{
      set_sub_cambios(sub_cambios+1);
    }
    for (var i = 0; i < sub_p.length; i++) {

      if(sub_p[i]==_old){
        _n.push(_new)

      }else{
        _n.push(sub_p[i])
      }
    }
    
    set_sub_p(_n)


  }

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <View style={styles.rootContainer}>
        <KeyboardAvoidingView
          style={styles.keyboardAvoidingView}
          behavior={Platform.OS === 'ios' ? 'position' : 'height'}
          enabled>
          <Animated.ScrollView
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: {
                      y: scrollY,
                    },
                  },
                },
              ],
              {
                useNativeDriver: true,
              },
            )}>
            <Animated.View
              style={[
                styles.coverPhotoContainer,
                {
                  transform: [
                    {
                      translateY: coverTranslateY,
                    },
                  ],
                },
              ]}>
              <Animated.Image
                source={{uri:img_url}}
                style={[
                  styles.coverPhoto,
                  {
                    transform: [
                      {
                        scale: coverScale,
                      },
                    ],
                  },
                ]}
              />
            </Animated.View>
            <HeadingInformation onSetTotal={updateSubProd} sub_p={sub_p} prods={()=>{ return prod();  }} data={detalles} />
            
            {doc.sub_productos != undefined &&
            <SubProductos
              data={detalles}
              doc= {doc}
              sub_p={sub_p}
              mod={sub_cambios}
              mod_crear={()=>{ set_sub_p(doc.sub_productos); set_sub_cambios(0);  }}
              sb_prod = {prod}
              onChange={change_su_prod}
              set_pre = {(val)=>{ }}
              addSideDishToBasket={addSideDishToBasket}
            />
            }

            <SideDishes
              data={detalles}
              doc= {doc}
              addSideDishToBasket={addSideDishToBasket}
            />


            <AddToBasketForm doc= {doc} updateTotalDishAmount={updateTotalDishAmount} />
          </Animated.ScrollView>
        </KeyboardAvoidingView>
        <View style={styles.addToBasketButtonContainer}>
          <Button
            childrenContainerStyle={styles.addToBasketButton}
            onPress={onAddToBasketButtonPressed}>
            <Text style={styles.addToBasketButtonText}>
              Añadir al carrito - {formatCurrency((totalPrice.total + (parseFloat(totalPrice.total)*Comicion/100)) + (sub_cambios*10) )}
            </Text>
          </Button>
        </View>
        <Animated.View
          style={[
            styles.header,
            {
              opacity: headerOpacity,
              backgroundColor: background,
            },
          ]}>
          <Text style={styles.headerTitle}>{doc.nombre}</Text>
        </Animated.View>
      </View>
    </SafeAreaView>
  );
};

export default DishDetails;
