import * as React from 'react';
import {View,InteractionManager} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {Container, Text, CheckBox} from '@src/components/elements';
import {Dish} from '@src/data/mock-places';
import styles from './styles';
import Api from '@src/Api'
import { useNavigation,useFocusEffect} from '@react-navigation/native';
import { LoadingIndicator} from '@src/components/elements';


const SideDishes: React.FC = ({id,sb_prod,onChange,set_pre}) => {
 

  var _id = id;
  const navigation = useNavigation();

  const _onAddItemButtonPressed = () => {
    
  };

  const [name,setName] = React.useState({nombre:'',precio:'',d:false,block:false});

  useFocusEffect(

    React.useCallback(() => {

      const task = InteractionManager.runAfterInteractions(() => {

        Api.Get('subproductos',id,(data)=>{

            setName({nombre:data.nombre,precio:data.precio,d:true,block:data.block})
            set_pre(data.precio)
        })
        
      })
      return () => task.cancel();
      },[name.nombre,id])
  )

  var press = ()=>{

    if(name.block!=true){
      navigation.navigate('SubProductos',{sb_prod:sb_prod,end:(item)=>{ 
          onChange(_id,item.objectId);  
          setName(item.nombre); 
          _id=item.objectId 
        }});
    }
  }
  
   var sim = ()=>{

    if(name.block!=true){
      return '>'
    }
    return "  "
  }

  return (
    <>
        <Text isPrimary style={styles.sectionTitle}>
          {sim()}
        
        </Text> 
        <Text onPress={()=>{ press() }} style={styles.sectionTitle}>{name.nombre}</Text>
        
   </>
  );
};

export default SideDishes;
