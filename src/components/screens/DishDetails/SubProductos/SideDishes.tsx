import * as React from 'react';
import {View,InteractionManager} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {Container, Text, CheckBox} from '@src/components/elements';
import {Dish} from '@src/data/mock-places';
import styles from './styles';
import Api from '@src/Api'
import { useNavigation,useFocusEffect} from '@react-navigation/native';
import { LoadingIndicator} from '@src/components/elements';
import Item from './Item';

type SideDishesProps = {
  data: Dish;
  addSideDishToBasket: (dish: Dish) => void;
  onChange:(amount: number) => void;
};

const SideDishes: React.FC<SideDishesProps> = ({
  data: {sideDishes},
  doc,
  addSideDishToBasket,
  sb_prod,
  onChange,
  set_pre,
  sub_p,
  mod,
  mod_crear
}) => {
  const {
    colors: {border},
  } = useTheme();

  const [data_list,set_data_list] = React.useState([]);
 
  var Reset = ()=>{
    if(mod!=0){
      return (<Text isPrimary>Limpiar modificaciones</Text>)
    }
  }
  
  return (
    <View>
      
        <View>
          <Text onPress={()=>{ mod_crear()  }} style={styles.sectionTitle}>Platillos {Reset()}</Text>
          {sub_p.map((dish, dishIndex) => (
            <Container
              key={dishIndex}
              style={[styles.dishItemContainer, {borderBottomColor: border}]}>
              <Container style={{flexDirection: 'row',
    justifyContent: 'space-between'}}>
                 
                 <Item onChange={onChange} set_pre={set_pre} sb_prod={sb_prod} id={dish} /> 
                  
              </Container>
            </Container>
          ))}
        </View>
      
    </View>
  );
};

export default SideDishes;
