import * as React from 'react';
import {View,InteractionManager} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {Container, Text, CheckBox} from '@src/components/elements';
import {Dish} from '@src/data/mock-places';
import styles from './styles';
import Api from '@src/Api'
import { useNavigation,useFocusEffect} from '@react-navigation/native';
import { LoadingIndicator} from '@src/components/elements';

type SideDishesProps = {
  data: Dish;
  addSideDishToBasket: (dish: Dish) => void;
};

const SideDishes: React.FC<SideDishesProps> = ({
  data: {sideDishes},
  doc,
  addSideDishToBasket,
}) => {
  const {
    colors: {border},
  } = useTheme();

  const [data_list,set_data_list] = React.useState([]);
   

  
  var check = (dish)=>{
      var che = false;

      var Importe = ()=>{
        if(dish.price==0){
          return "";
        }else{
          return "+ $"+dish.price
        }
      }
      return (<CheckBox
                  label={dish.title}
                  onPress={(val)=>{
                    
                    if(val){
                      Api.Extras++;  
                    }else{
                      Api.Extras--; 
                    }

                    
                    if('limit_extras' in doc){
                        
                        if(Api.Extras>parseInt(doc.limit_extras)){
                            Api.Extras--;
                            alert('Máximo '+doc.limit_extras)
                            return false ;
                        }
                    }
                    
                    
                    addSideDishToBasket(dish)
                    return val
                  }
                  }
                  rightElement={<Text>{Importe()}</Text>}
                  value = {che}
                />)
  }

  const onCheckBoxPress = (selectedDish: Dish) => {
    return () => {
      addSideDishToBasket(selectedDish);
    };
  };
  
  return (
    <View>
      {sideDishes.map((section, sectionIndex) => (
        <View key={sectionIndex}>
          <Text style={styles.sectionTitle}>{section.title}</Text>
          {section.data.map((dish, dishIndex) => (
            <Container
              key={dishIndex}
              style={[styles.dishItemContainer, {borderBottomColor: border}]}>
              <Container style={styles.checkBoxContainer}>
                  {check(dish)}
              </Container>
            </Container>
          ))}
        </View>
      ))}
    </View>
  );
};

export default SideDishes;
