import * as React from 'react';
import {View,InteractionManager} from 'react-native';
import {Container, Text} from '@src/components/elements';
import {Dish} from '@src/data/mock-places';
import styles from './styles';
import Api from '@src/Api'
import { useNavigation,useFocusEffect} from '@react-navigation/native';
import {formatCurrency} from '@src/utils/number-formatter';

type HeadingInformationProps = {
  data: Dish;
};

const HeadingInformation: React.FC<HeadingInformationProps> = ({data,sub_p,prods,onSetTotal}) => {
  const {title, price, description} = data;

  const [total,set_total] = React.useState(price);
  const [Comicion,setComicion] = React.useState(0);
    
    var d = prods();
    useFocusEffect(
    React.useCallback(() => {
     
      Api.Parametros('2002',(param)=>{
        setComicion(parseFloat(param))
      })
      const task = InteractionManager.runAfterInteractions(() => {
        
        if(d.length!=0){
          
          Api.GetMulti('subproductos',d,(docs)=>{
          var _total = 0;
          for (var i = 0; i < docs.length; i++) {
            _total += parseFloat(docs[i].precio)
          }
            
           set_total(_total)
           onSetTotal(_total)
         })
         
            
        }
         

         });

      return () => task.cancel();
    }, d),
  );


  return (
    <Container style={styles.headingContainer}>
      <Container style={styles.titleContainer}>
        <Text style={styles.title}>{title}</Text>
        <Text isPrimary style={styles.title}>
          {formatCurrency(parseFloat(total)+(parseFloat(total)*Comicion/100))}
        </Text>
      </Container>
      <Text style={styles.description}>{description}</Text>
    </Container>
  );
};

export default HeadingInformation;
