import * as React from 'react';
import {
  Container,
  Text,
  TextField,
  Button,
  Icon,
} from '@src/components/elements';
import styles from './styles';

type AddToBasketFormProps = {
  updateTotalDishAmount: (amount: number) => void;
  doc:{}
};

const AddToBasketForm: React.FC<AddToBasketFormProps> = ({
  updateTotalDishAmount,doc
}) => {
  var def = 1;

  if(doc.minimo!=undefined){
    def = parseInt(doc.minimo)
  }
  const [totalAmount, setTotalAmount] = React.useState(def);
  const onButtonPressed = (amount: number) => {

    
    return () => {

      if(parseInt(doc.minimo) > (totalAmount + amount)){
      alert('Mínimo '+doc.minimo+' '+doc.notas_cantidad)
      return;
      }

      if (totalAmount === 1 && amount < 1) {
        return;
      }
      const newTotalAmount = totalAmount + amount;
      setTotalAmount(newTotalAmount);
      updateTotalDishAmount(newTotalAmount);
    };
  };

  return (
    <Container style={styles.formContainer}>
      <Text style={styles.title}>Notas</Text>
      <Text style={{color:'#FFA500'}}>{doc.notas}</Text>
      <TextField
        containerStyle={styles.textField}
        placeholder="ejemplo: Sin cebollas, por favor"
      />
      <Container style={styles.buttonGroupSection}>
        <Container style={styles.buttonGroupContainer}>

          <Button style={styles.button} onPress={onButtonPressed(-1)}>
            <Icon name="minus" isPrimary />
          </Button>
          
          <Text style={styles.amount}>{totalAmount} {doc.notas_cantidad}</Text>
          <Button style={styles.button} onPress={onButtonPressed(1)}>
            <Icon name="plus" isPrimary />
          </Button>
        </Container>
      </Container>
    </Container>
  );
};
export default AddToBasketForm;
