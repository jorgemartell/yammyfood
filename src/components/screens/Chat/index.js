import * as React from 'react';
import {I18nManager, ScrollView, View,SafeAreaView,Image,TouchableOpacity} from 'react-native';
import {
  Text,
  Section,
  Icon,
  Container,
  Button,
   TextField,
  Divider,
} from '@src/components/elements';
import Api from '@src/Api'
import { GiftedChat,Send } from 'react-native-gifted-chat'
import * as ImagePicker from "react-native-image-picker"
import ListRowItem from '@src/components/elements/List/ListRowItem';
import LottieView from 'lottie-react-native';
import styles from './styles';
import storage from '@react-native-firebase/storage';
import uuid from 'react-native-uuid';


type SupportCenterPros = {};

const SupportCenter: React.FC<SupportCenterPros> = () => {
  


  const [data, setData] = React.useState([]);
  const [uri, seturi] = React.useState();
  const [upload_file, set_upload_file] = React.useState(false);
  
  const [sms_input, Setsms_input] = React.useState('');

  React.useEffect(() => {
    Api.ChatGet((val)=>{ 


      if(val!=null){
      
        var ert = []

        Object.keys(val).forEach(function(key) {

          
          ert.push(key)
          
        })



        var rty = ert.sort();

        var r = []
        for (var i = ert.length - 1; i >= 0; i--) {
          r.push(val[ert[i]])
        }
        setData(r)
      }else{
        //setData([])
      }
      
    })
  },[])


  var handleChoosePhoto = () => {
    var options = {
   title: 'Select Image',
   customButtons: [
     {
       name: 'customOptionKey',
       title: 'Choose Photo from Custom Option'
     },
   ],
   storageOptions: {
     skipBackup: true,
     path: 'images',
   },
};
    ImagePicker.launchImageLibrary(options, response => {
      
      set_upload_file(true)
      const reference = storage().ref('/chat/'+Api.UserId+'/'+response.fileName);

      const uploadUri = Platform.OS === 'ios' ? response.uri.replace('file://', '') : response.uri;
      var er = reference.putFile(uploadUri);
      er.then(async () => {

        var img = "https://firebasestorage.googleapis.com/v0/b/delivery-23f4e.appspot.com/o/chat%2F"+Api.UserId+"%2F"+response.fileName+"?alt=media&token=339fb0c9-b1ee-4c7f-9991-32933eb87db2"
        Api.ChatSend({
          _id:uuid.v4(),
          image:img,
          user:{ _id: 1}
        });
        set_upload_file(false)
      });

    })
  }


  
  return (
    
    <GiftedChat
      messages={data}
      placeholder="Escribe un mensaje ..."
      renderSend={(props)=>{  return (<><TouchableOpacity  onPress={()=>{ handleChoosePhoto() }}><Image source={require('@src/assets/chat/img.png')} resizeMode={'center'}/></TouchableOpacity><Send
                {...props}
            ><View style={{marginRight: 20, marginBottom: 0}}><Image source={require('@src/assets/chat/send.png')} resizeMode={'center'}/></View></Send></>) }}
      onSend={newMessage => { Api.ChatSend(newMessage[0]); }}
      user={{ _id: 1,name: 'React Native',
          avatar: 'https://placeimg.com/140/140/any', }}
    />
  );
};

export default SupportCenter;
