import {StyleSheet} from 'react-native';
export default StyleSheet.create({
    root: {
    flex: 1,
  },
  contactsupportCenterContainer: {
    flexDirection: 'row',
    paddingLeft: 15,
    paddingRight: 15,
    margin: 10,

    borderRadius: 15,
    position:'relative'
  },
  supportCenterLeft: {
    flex: 1,
    justifyContent: 'center',
  },
  supportCenterRight: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    position:'absolute',
    right:0,
    top:15
  },
  supportCenterButton: {
    width: 100,
    marginTop: 10,
  },
  supportCenterLottieView: {
    width: '100%',
  },
});
