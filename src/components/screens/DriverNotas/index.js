import * as React from 'react';
import {I18nManager, ScrollView, View,SafeAreaView} from 'react-native';
import {
  Section,
  Icon,
  Container,
  Text,
  Button,
   TextField,
  Divider,
} from '@src/components/elements';
import Api from '@src/Api'
import CartContext from '@src/context/cart-context';
import {useTheme, useNavigation} from '@react-navigation/native';
import ListRowItem from '@src/components/elements/List/ListRowItem';
import LottieView from 'lottie-react-native';
import styles from './styles';

type SupportCenterPros = {};

const SupportCenter: React.FC<SupportCenterPros> = () => {
  


  const {driverNotes,setDriverNotes} = React.useContext(CartContext);
  const [notas, setnotas] = React.useState(driverNotes);
  
  
  
  const {goBack} = useNavigation();



  return (

    <SafeAreaView style={styles.root}>



    <Container style={styles.contactsupportCenterContainer}>
      <Text>Agregar una nota al conductor</Text>

      <TextField value={notas} onChangeText={(val)=>{ setnotas(val) }}  containerStyle={{marginTop:10,marginBottom:10}}
        
        placeholder=""
      />
      <View >
      
      <Button onPress={()=>{ setDriverNotes(notas);goBack(); }} >
            <Text>Enviar</Text>
          </Button>
      </View>
      </Container>
      
    </SafeAreaView>
  );
};

export default SupportCenter;
