import * as React from 'react';
import {View,InteractionManager} from 'react-native';
import {List,Text} from '@src/components/elements';
import {mockPlaceList} from '@src/data/mock-places';
import PlaceListItem from '@src/components/common/PlaceListItem';
import Api from '@src/Api'
import database from '@react-native-firebase/database';
import DishItem from '@src/components/common/DishItem';
import { useNavigation,useFocusEffect} from '@react-navigation/native';
import { LoadingIndicator} from '@src/components/elements';
import firestore from '@react-native-firebase/firestore';




var Item = ({res_id,er})=>{
	const navigation = useNavigation();
	const [D,setD] = React.useState((<Text>...</Text>));

	useFocusEffect(
    	React.useCallback(() => {
      	const task = InteractionManager.runAfterInteractions(() => {
		Api.Get('empresas',res_id,(res_doc)=>{
					setD(<DishItem OnAddGoTo={()=>{ navigation.navigate('PlaceDetailsScreen',{id:res_id,doc:res_doc});  }} data={er} res={res_doc} />)
						
					})
				})

      return () => task.cancel();
  		},[]))


	return D
}



const FindResult = ({SearchText}) => {


	
	const [get_list,set_list] = React.useState([]);
	const [Load,SetLoad] = React.useState(true);

	const navigation = useNavigation();



		useFocusEffect(
    	React.useCallback(() => {
      	const task = InteractionManager.runAfterInteractions(() => {
      		var l = []


      		SetLoad(true)
      		
      		Api.List('productos',data => {

      			
      		


        		var docs = data.filter(function(item){
				   return item.nombre.toLowerCase().includes(SearchText.toLowerCase());
				}).map(function(item){
				    return item;
				});
        		
        		if(docs.length==0){
        			SetLoad(false)
        		}
       			


				var list = []
				for (var i = 0; i < docs.length; i++) {
				
				try {
				var img_url = "https://firebasestorage.googleapis.com/v0/b/delivery-23f4e.appspot.com/o/"+docs[i].img+"?alt=media"
				var er = {
					doc:docs[i],
					objectId: docs[i].objectId,
					title: docs[i].nombre,
					description: docs[i].descripcion,
					price: docs[i].precio,
					image: {uri:img_url},
					}
					const res_id = docs[i].empresa.objectId
				
					list.push(<Item res_id={res_id} er={er} />)      	
					 //Set(res_id,er)   		
					
					 } catch (error) {
						  console.error(error);
						  SetLoad(false)
						  // expected output: ReferenceError: nonExistentFunction is not defined
						  // Note - error messages will vary depending on browser
						}
					
								
				}

				set_list(list)
				SetLoad(false)


    		})

	
      })

      return () => task.cancel();
  		},[SearchText]))

	

	if(Load){

		return (<LoadingIndicator size="large" hasMargin />)
	}

	if(get_list.length==0){

		return (<Text>No hay resultados....</Text>)
	}

  return (
    <View>
      {get_list}
    </View>
  );
};

export default FindResult;
