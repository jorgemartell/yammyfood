import * as React from 'react';
import {savedAddresses, Address} from '@src/data/mock-address';
import List from '@src/components/elements/List/List';
import {Icon, Divider} from '@src/components/elements';
import ListRowItem, {
  ListRowItemProps,
} from '@src/components/elements/List/ListRowItem';
import {useNavigation} from '@react-navigation/native';
import CartContext from '@src/context/cart-context';
import {View,Platform,I18nManager,PermissionsAndroid} from 'react-native';
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  LatLng,
  MapEvent,
  Region,
} from 'react-native-maps';
import Geolocation from 'react-native-geolocation-service';
import SelectLocation from '@src/components/screens/SelectLocation/SelectLocation'
import styles from './styles';
import Api from '@src/Api';


type ChangeAddressProps = {};
const chevronIconName = I18nManager.isRTL ? 'chevron-left' : 'chevron-right';

const savedPlaceListItem: ListRowItemProps = {
  id: '1',
  title: 'Saved Places',
  subTitle: 'Select a delivery address easily',
  leftIcon: <Icon name="bookmark" size={16} />,
  rightIcon: <Icon name={chevronIconName} size={16} />,
};

const useCurrentLocationListItem: ListRowItemProps = {
  id: '1',
  title: 'Usar ubicación actual',
  subTitle: '588 Blanda Square - Virginia',
  leftIcon: <Icon name="crosshairs" size={16} />,
};

const ChangeAddress: React.FC<ChangeAddressProps> = (data) => {
  const navigation = useNavigation();
  const [currentLocation, setCurrentLocation] = React.useState<Region>({
    longitude: 0,
    latitude: 0,
    longitudeDelta: 0.0022,
    latitudeDelta: 0.0031,
  });
  const {SetAddres,Addres} = React.useContext(CartContext);

const _onRegionChangeComplete = (region: Region) => {
    setCurrentLocation(region);
  };

  const _initUserLocation = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        const {longitude, latitude} = position.coords;
        setCurrentLocation((location) => {
          setMarkerLocation({
            longitude,
            latitude,
          });
          return {
            ...location,
            longitude,
            latitude,
          };
        });
      },
      (error) => {
        console.log(error.code, error.message);
      },
      {timeout: 15000, maximumAge: 10000},
    );
  };

  React.useEffect(() => {
    const requestAndroidLocationPermission = async () => {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Food Delivery App Permission',
          message:
            'Food Delivery App needs access to your location ' +
            'so you see where you are on the map.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        _initUserLocation();
      } else {
        console.log('Camera permission denied');
      }
    };

    if (Platform.OS === 'android') {
      requestAndroidLocationPermission();
    } else {
      _initUserLocation();
    }
  }, []);
  const _prepareListData = (addresses: Address[]) => {
    return addresses.map((item) => {
      const {id, description, name} = item;
      return {
        id,
        title: name,
        subTitle: description,
        rightIcon: <Icon name="bookmark" size={16} />,
      };
    });
  };

  const _savedPlaceListItemPressed = () => {
    navigation.navigate('SavedAddressesScreen');
  };
  const [markerLocation, setMarkerLocation] = React.useState<LatLng>();
  const _renderListHeader = () => {
    return (
      <>
        <ListRowItem
          {...savedPlaceListItem}
          onPress={_savedPlaceListItemPressed}
        />
        <Divider />
        <ListRowItem {...useCurrentLocationListItem} />
        <Divider />
      </>
    );
  };
  const _onMapViewPressed = (event: MapEvent) => {


    const {
      nativeEvent: {
        coordinate: {latitude, longitude},
      },
    } = event;
      SetAddres(longitude,latitude)
    
  };
  const _onMarkerDragEd = (event: MapEvent) => {
    setMarkerLocation(event.nativeEvent.coordinate);
  };
  return (<View style={styles.rootView}>
      <MapView
        loadingEnabled
        provider={PROVIDER_GOOGLE}
        toolbarEnabled
        showsUserLocation
        showsMyLocationButton
        style={styles.mapView}
        zoomControlEnabled
        onRegionChangeComplete={_onRegionChangeComplete}
        region={Addres}
        onPress={_onMapViewPressed}>
       
          <Marker
            draggable
            coordinate={Addres}
            onDragEnd={_onMarkerDragEd}
          />
        
      </MapView>
    </View>)
  /*
  return (
    <List
      data={_prepareListData(savedAddresses)}
      ListHeaderComponent={_renderListHeader()}
    />
  );*/
};

export default ChangeAddress;
