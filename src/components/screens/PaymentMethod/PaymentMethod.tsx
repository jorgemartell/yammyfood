import * as React from 'react';
import {ScrollView,Image} from 'react-native';
import {useTheme, useNavigation,useFocusEffect} from '@react-navigation/native';
import {Section, RadioButton, Icon} from '@src/components/elements';
import {RadioOption} from '@src/components/elements/RadioButton/RadioButton';
import {paymentMethods} from '@src/data/mock-payment-method';
import Api from '@src/Api'
import {LoadingIndicator} from '@src/components/elements';
import { View,InteractionManager} from 'react-native';
import CartContext from '@src/context/cart-context';

type PaymentMethodProps = {  };

const PaymentMethod: React.FC<PaymentMethodProps> = ({ route }) => {

  const navigation = useNavigation();
  const { addpayment } = route.params;

   const [load,set_load] = React.useState(true);
   const [data,set_data] = React.useState([]);
    const {addPayment} = React.useContext(CartContext);


  const _data: RadioOption[] = paymentMethods.map((item) => {
    const {objectId, name, icon} = item;
    return {
      label: name,
      value: objectId,
      rightElement: <Icon name={icon} size={20} />,
    };
  });

    useFocusEffect(
    React.useCallback(() => {
      const task = InteractionManager.runAfterInteractions(() => {
        
        Api.List('metodopago',(_data)=>{
          var re = [];
          for (var i = 0; i < _data.length; i++) {
            var row = _data[i]
            var img_url = "https://firebasestorage.googleapis.com/v0/b/delivery-23f4e.appspot.com/o/"+row.img+"?alt=media"
            re.push({label:row.nombre,value:row.objectId,rightElement: <Image source={{uri:img_url}} /> })
          }
          set_data(re)
          set_load(false)
        })
      });
      return () => task.cancel();
    }, []),
  );

  const _onItemPressed = (item: RadioOption) => {

   addpayment(item)
   navigation.goBack()
  };

  if(load){
    return (<LoadingIndicator size="large" hasMargin />)
  }

  return (

    <ScrollView>
      <Section title="Elija un método de pago">
        <RadioButton data={data} onItemPressed={_onItemPressed} />
      </Section>
    </ScrollView>
  );
};

export default PaymentMethod;
