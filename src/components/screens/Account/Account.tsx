import * as React from 'react';
import {
  Container,
  Icon,
  Divider,
  SearchBar,
  Button,
  Text,
} from '@src/components/elements';
import {
  ScrollView,
  Image,
  View,
  Alert,
  AlertButton,
  I18nManager,
} from 'react-native';
import ListRowItem from '@src/components/elements/List/ListRowItem';
import {profile} from '@src/data/mock-profile';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import AuthContext from '@src/context/auth-context';
import Api from '@src/Api';;
import auth from '@react-native-firebase/auth';

type AccountProps = {};

const Account: React.FC<AccountProps> = () => {
  const navigation = useNavigation();
  const {signOut,userToken} = React.useContext(AuthContext);
  const chevronIconName = I18nManager.isRTL ? 'chevron-left' : 'chevron-right';

 
  const alertButtons: AlertButton[] = [
    {
      text: 'Cancelar',
      style: 'cancel',
    },
    {text: 'OK', onPress: () => { Api.DeleteUserId(); signOut(); navigation.navigate('Restaurantes');  }},
  ];

  const onLogoutButtonPressed = () => {
    Alert.alert('Confirmar', '¿Estás seguro de que quieres cerrar sesión?', alertButtons);
  };

  return (
    <ScrollView>
      
      
      <Container>
        <ListRowItem
          title={Api.UserData.nombre}
          subTitle="Editar perfil"
          onPress={() => navigation.navigate('EditProfileScreen')}
          
          rightIcon={<Icon name={chevronIconName} />}
        />
      </Container>
      <Container style={styles.accountMenuItemContainer}>
        <Divider />
        <Divider />
        <ListRowItem
          title="Historial de pedidos"
          onPress={() => navigation.navigate('OrderHistoryScreen')}
          rightIcon={<Icon name={chevronIconName} />}
        />
        
        { false &&
        <ListRowItem
          title="Dirección de entrega"
          onPress={() => navigation.navigate('SavedAddressesScreen')}
          rightIcon={<Icon name={chevronIconName} />}
        /> }
        <Divider />
        <ListRowItem
          title="Ajustes"
          onPress={() => navigation.navigate('SettingsScreen')}
          rightIcon={<Icon name={chevronIconName} />}
        />
        <Divider />

        <ListRowItem
          title="Centro de Apoyo"
          onPress={() => navigation.navigate('SupportCenterScreen')}
          rightIcon={<Icon name={chevronIconName} />}
        />
        
        { false && <ListRowItem
          title="Share Feedback"
          rightIcon={<Icon name={chevronIconName} />}
        />}
      </Container>
       <View style={styles.buttonContainer}>
        <Button isFullWidth isTransparent onPress={onLogoutButtonPressed}>
          <Text isBold isPrimary>
            Salir
          </Text>
        </Button>
      </View>
    </ScrollView>
  );
};

export default Account;
