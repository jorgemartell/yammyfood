import * as React from 'react';
import {Animated, SafeAreaView, View,InteractionManager} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {Container, Text,SearchBar} from '@src/components/elements';
import HeadingInformation from './HeadingInformation';
import PopularDishes from './PopularDishes';
import TabSectionList from '@src/components/elements/TabSectionList';
import DishItem from '@src/components/common/DishItem';
import {mockPlaceDetails} from '@src/data/mock-places';
import styles from './styles';
import BasketSummary from './BasketSummary';
import { LoadingIndicator} from '@src/components/elements';
import Api from '@src/Api'
import { useNavigation,useFocusEffect} from '@react-navigation/native';



type PlaceDetailsProps = {};

const PlaceDetails: React.FC<PlaceDetailsProps> = (data) => {
  const {
    colors: {primary, border, card},
  } = useTheme();
  const [scrollY] = React.useState(new Animated.Value(0));
  const [load,set_load] = React.useState(true);
  const [categorias,set_categorias] = React.useState([]);
  const [t_search,set_t_search] = React.useState("");

  const coverTranslateY = scrollY.interpolate({
    inputRange: [-4, 0, 10],
    outputRange: [-2, 0, 3],
  });

  const coverScale = scrollY.interpolate({
    inputRange: [-200, 0],
    outputRange: [2, 1],
    extrapolateRight: 'clamp',
  });

  const tabBarOpacity = scrollY.interpolate({
    inputRange: [200, 500],
    outputRange: [0, 1],
    extrapolate: 'clamp',
  });
  var tabs = []

  var GetData = (find_txt)=>{

    Api.Parametros('2002',(param)=>{
        var comicion = parseFloat(param);

        Api.Query('categorias',{ where: {objectId:{"$in":data.route.params.doc.categoria}},order:'nombre' },(docs)=>{
           

           var res = []
           for (var e = 0; e < docs.length; e++) {
               
           
            for (var i = 0; i < data.route.params.doc.categoria.length; ++i) {
              var categoria = data.route.params.doc.categoria[i]
              if(categoria==docs[e].objectId){
                res.push({objectId:docs[e].objectId,title:docs[e].nombre,data:[]})
                
              }    
              
            }

          }

          

          
          Api.Query('productos',{where:{empresa: {"__type":"Pointer","className":"empresas","objectId":data.route.params.doc.objectId} },order:'nombre'},(docs)=>{
            for (var i = 0; i < docs.length; i++) {
              
              for (var e = 0; e < res.length; ++e) {


                if(res[e].objectId==docs[i].categoria.objectId){
                  

                  

                    var n = true
                    if(find_txt!=""){
                      n= docs[i].nombre.includes(find_txt);
                    }
                    var img_url = "https://firebasestorage.googleapis.com/v0/b/delivery-23f4e.appspot.com/o/"+docs[i].img+"?alt=media"
                    var d = {
                      doc:docs[i],
                      objectId: docs[i].objectId,
                      title: docs[i].nombre,
                      description: docs[i].descripcion,
                      price: parseFloat(docs[i].precio)+(parseFloat(docs[i].precio)*comicion/100),
                      image: {uri:img_url},
                    }
                    if(n){
                      res[e].data.push(d)
                    }
                    

                  
                }
              }
            }


            var end = []
            for (var i = 0; i < res.length; ++i) {
              if(res[i].data.length!=0){
                end.push(res[i])
              }
            }
            set_load(false)


            set_categorias(end)


          })
          
        

        })
      })

  }
  var Find = (text)=>{

    set_t_search(text)
    GetData(text)
  }
  useFocusEffect(
    React.useCallback(() => {
      const task = InteractionManager.runAfterInteractions(() => {
        GetData("")

        
      });
      return () => task.cancel();
    }, []),
  );

  if(load){
    return (<LoadingIndicator size="large" hasMargin />)
  }
  var img_url = "https://firebasestorage.googleapis.com/v0/b/delivery-23f4e.appspot.com/o/"+data.route.params.doc.img_header+"?alt=media"
  return (
    <SafeAreaView style={styles.rootContainer}>
      <View style={styles.tabSectionListContainer}>
        <TabSectionList
          ListHeaderComponent={
            <>
              <Animated.View
                style={[
                  styles.coverPhotoContainer,
                  {
                    transform: [
                      {
                        translateY: coverTranslateY,
                      },
                    ],
                  },
                ]}>
                {mockPlaceDetails.coverImage && (
                  <Animated.Image
                    source={{uri:img_url}}
                    style={[
                      styles.coverPhoto,
                      {
                        transform: [
                          {
                            scale: coverScale,
                          },
                        ],
                      },
                    ]}
                  />
                )}
           </Animated.View>
              <HeadingInformation doc={data.route.params.doc} data={mockPlaceDetails} />
              <SearchBar onChangeText={(text)=>{ Find(text) }} placeholder="Encuentra lugares, platillos o restaurantes ..." />
            </>
          }
          sections={categorias || []}
          keyExtractor={(item) => item.title}
          stickySectionHeadersEnabled={false}
          scrollToLocationOffset={5}
          tabBarStyle={[styles.tabBar, {opacity: tabBarOpacity}]}
          tabBarScrollViewStyle={{backgroundColor: card}}
          ItemSeparatorComponent={() => (
            <Container style={[styles.separator, {backgroundColor: border}]} />
          )}
          renderTab={({title, isActive}) => {
            const borderBottomWidth = isActive ? 2 : 0;
            return (
              <Container
                style={{
                  borderBottomWidth,
                  borderColor: primary,
                }}>
                <Text isPrimary={isActive && true} style={styles.tabText}>
                  {title}
                </Text>
              </Container>
            );
          }}
          renderSectionHeader={({section}) => (
            <Text style={styles.sectionHeaderText}>{section.title}</Text>
          )}
          renderItem={({item}) => {
            return <DishItem data={item} res={data.route.params.doc} />;
          }}
          onScroll={Animated.event(
            [
              {
                nativeEvent: {
                  contentOffset: {
                    y: scrollY,
                  },
                },
              },
            ],
            {
              useNativeDriver: true,
            },
          )}
        />
      </View>
      <BasketSummary doc={data.route.params.doc} da="ddddd" />
    </SafeAreaView>
  );
};
export default PlaceDetails;
