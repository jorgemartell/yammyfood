import * as React from 'react';
import {Card, Container, Icon, Text} from '@src/components/elements';
import PlaceCardInfo from '@src/components/common/PlaceCardInfo';
import styles from './styles';
import {Place} from '@src/data/mock-places';

type HeadingInformationProps = {
  data: Place;
  doc:{}
};

const HeadingInformation: React.FC<HeadingInformationProps> = ({doc,data}) => {
  const {title, subTitle} = data;
  /*
    <Container style={styles.infoSection}>
        <Text style={styles.label}>Coupon Codes</Text>
        <Container>
          <Container style={styles.coupon}>
            <Icon name="tag" style={styles.couponIcon} isPrimary />
            <Text isPrimary>35% off for Cheese Burger</Text>
          </Container>
          <Container style={styles.coupon}>
            <Icon name="tag" style={styles.couponIcon} isPrimary />
            <Text isPrimary>5% off for all items</Text>
          </Container>
        </Container>
        
      </Container>
  */
  return (
    <Card
      isSmallCover
      title={doc.nombre}
      subTitle={doc.descripcion}
      titleStyle={styles.title}
      style={styles.headingContainer}>
      <PlaceCardInfo data={data} doc={doc} />
      <Container style={styles.infoSection}>
        <Text style={styles.label}>Horario de apertura</Text>
        <Text>{doc.horario}</Text>
      </Container>
      

    </Card>
  );
};

export default HeadingInformation;
