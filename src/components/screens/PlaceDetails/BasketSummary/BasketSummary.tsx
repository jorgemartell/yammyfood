import * as React from 'react';
import {View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Container, Button, Text} from '@src/components/elements';
import CartContext from '@src/context/cart-context';
import {formatCurrency} from '@src/utils/number-formatter';
import styles from './styles';

type BasketSummaryProps = {};

const BasketSummary: React.FC<BasketSummaryProps> = ({doc}) => {
  const {cartItems, totalPrice} = React.useContext(CartContext);
  const navigation = useNavigation();


  const _onViewBasketButtonPressed = () => {
    
    navigation.navigate('CheckoutScreen',{restaurante:doc});
  };

  var sub_total = 0;
  var art = 0;
  for (var i = 0; i < cartItems.length; i++) {
    var item = cartItems[i];

    if(item.res==doc.objectId){
      art++;
      sub_total = sub_total+item.subtotal;
    }
  }

  return (
    <>
      {art > 0 && (
        <Container style={styles.viewBasketButtonContainer}>
          <Button
            childrenContainerStyle={styles.viewBasketButton}
            onPress={_onViewBasketButtonPressed}>
            <View style={styles.viewBasketButtonTextContainer}>
              <Text isBold style={styles.viewBasketButtonText}>
                Ver orden
              </Text>
              <Text style={styles.cardItemText}>{`${art} ${
                art > 1 ? 'Artículos' : 'Articulo'
              }`}</Text>
            </View>
            <Text style={styles.totalPriceText} isBold>
              {formatCurrency(sub_total)}
            </Text>
          </Button>
        </Container>
      )}
    </>
  );
};

export default BasketSummary;
