import * as React from 'react';
import  {useContext} from 'react';
import {SafeAreaView, View, ScrollView, Alert,Picker} from 'react-native';
import {Text, TextField, Button, Dialog} from '@src/components/elements';
import useThemeColors from '@src/custom-hooks/useThemeColors';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import Api from '@src/Api';
import AuthContext from '@src/context/auth-context';
import { AsyncStorage } from 'react-native';

type AuthWithPhoneNumberProps = {};

const AuthWithPhoneNumber: React.FC<AuthWithPhoneNumberProps> = (param) => {
  const navigation = useNavigation();
  const {card} = useThemeColors();
  const [phoneNumber, setPhoneNumber] = React.useState('');
  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const [confirm, setConfirm] = React.useState(null);
  const [code, setCode] = React.useState('');

  const [selectedValue, setSelectedValue] = React.useState("+52");
  const {userToken,signIn} = useContext(AuthContext);

  const _onPhoneNumberFieldChange = (value: string) => {
    setPhoneNumber(value);
  };
  Api.Conect()
  async function signInWithPhoneNumber(phoneNumber) {


    Api.Save('phone_login',{to:phoneNumber},async (id)=>{
      _hideModal();
      navigation.navigate('AuthVerificationCodeScreen',{phone:phoneNumber,confirm:(codigo)=>{

        Api.Query('phone_login',{where:{objectId:id,codigo:parseInt(codigo)}},(data)=>{
          if(data.length==0){
            Alert.alert('Error', 'Codigo invalido.'); 
          }else{
           Api.SetUserId(data[0].usuario.objectId)
           signIn();
           navigation.goBack();
           if('login' in param.route.params){
               param.route.params.login();
           }
           

          }
        })

      }});
    })
    
  }
  async function confirmCode() {
    try {
      await confirm.confirm(code);
    } catch (error) {
      alert('Invalid code.');
    }
  }
  const _hideModal = () => {
    setIsModalVisible(false);
  };








  const _onNextButtonPressed = () => {
    if (!phoneNumber) {
      Alert.alert('Error', '¡Por favor, introduzca su número de teléfono!');
      return;
    }
    
    setIsModalVisible(true);
  };
  const _onConfirmButtonPressed = () => {
    signInWithPhoneNumber(selectedValue+phoneNumber)

    
  };

    var input = {width: 250, height: 200,paddingTop:10}
   if (Platform.OS === 'android') {
     input = {width: 250, height: 50}
   }
  return (
    <SafeAreaView style={styles.root}>
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.formContainer}>
          <Text isBold isHeadingTitle>
           
        Ingrese su número telefónico
          </Text>
          <Text isSecondary hasMargin>
            
Ingrese su número de teléfono para utilizar nuestros servicios indicando la lada su pais " +52 "
          </Text>





          <View style={{flex: 1, flexDirection: 'row'}}>
        <View style={{width: 100, height: 50,paddingTop:15}}>
          <Picker
          selectedValue={selectedValue}
          onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
        style={{ color:'#ffffff'}}
      >
        <Picker.Item label="MX +52" value="+52" />
        <Picker.Item label="US +1" value="+1" />
      </Picker>
        </View>
        <View style={input}>
        
           <TextField
            style={[{backgroundColor: card}, styles.phoneNumberTextField]}
            value={phoneNumber}
            onChangeText={_onPhoneNumberFieldChange}
            hasMargin
            placeholder="
Ingrese su número telefónico"
            keyboardType="phone-pad"
            autoFocus
          />

        </View>
      </View>



          

         
        </View>
        <Button isFullWidth onPress={_onNextButtonPressed}>

          <Text isBold>Siguiente</Text>
        </Button>
      </ScrollView>
      <Dialog isVisible={isModalVisible} onBackdropPress={_hideModal}>
        <Text isCenter>Iniciar sesión con número de teléfono</Text>
        <Text isHeadingTitle isCenter isBold style={styles.phoneNumberText}>
          {selectedValue} {phoneNumber}
        </Text>
        <Text isCenter>
          
Enviaremos el código de autenticación al número de teléfono que ingresó.
        </Text>
        <Text isCenter>¿Quieres continuar?</Text>
        <View style={styles.confirmButtonContainer}>
          <Button isFullWidth onPress={_onConfirmButtonPressed}>
            <Text isBold>Confirmar</Text>
          </Button>
        </View>
        <View style={styles.cancelButtonContainer}>
          <Button isFullWidth isTransparent onPress={_hideModal}>
            <Text>Cancelar</Text>
          </Button>
        </View>
      </Dialog>
    </SafeAreaView>
  );
};

export default AuthWithPhoneNumber;
