import * as React from 'react';
import {View, Image} from 'react-native';
import {Container, Text, Button} from '@src/components/elements';
import AuthContext from '@src/context/auth-context';
import useThemeColors from '@src/custom-hooks/useThemeColors';
import styles from './styles';
import {useNavigation,StackActions} from '@react-navigation/native';
import AuthWithPhoneNumber from '@src/components/screens/AuthWithPhoneNumber';
import auth from '@react-native-firebase/auth';
import Api from '@src/Api'
import { AsyncStorage } from 'react-native';


type AuthenticationProps = {};

const Authentication: React.FC<AuthenticationProps> = (param) => {
  const navigation = useNavigation();
  const {primary} = useThemeColors();
  const {signIn} = React.useContext(AuthContext);
  const [user, setUser] = React.useState();
  const [test, setTest] = React.useState(false);
  const [initializing, setInitializing] = React.useState(true);

  Api.Conect()
  const _onConnectWithPhoneNumberButtonPressed = () => {
    navigation.navigate('AuthWithPhoneNumberScreen');
  };

  const testMode = ()=>{


  

  }
  const _onSocialNetworkConnectButtonPressed = () => {
    signIn();
  };


  



  
  var appIcon = styles.appIcon

   if (Platform.OS === 'android') {
     appIcon =    {
    width: '60%',
    height: '75%',
    resizeMode: 'stretch',
  }
   }

  return (
    <View
      style={[
        styles.container,
        {
          backgroundColor: primary,
        },
      ]}>
      <View style={styles.appIconContainer}>
        <Image
          source={require('../../../assets/app/app_icon.png')}
          style={appIcon}
        />
      </View>
      <Container style={styles.loginMethodContainer}>
        <Text isBold isHeadingTitle>
           Consigue a domicilio la comida más rica de Mexicali en segundos.
        </Text>
        <Text isSecondary style={styles.introductionText}>
           Ordena al instante los platillos más ricos de los mejores restaurantes de Mexicali. Entregas rápidas y a un mejor precio.
        </Text>
        
        
        <View style={styles.loginMethod}>
          <Button
            style={styles.button}
            isFullWidth
            onPress={()=>{ navigation.navigate('AuthWithEmail'); }}>
            <Text isBold isWhite>
              
Conectate con tu correo electrónico
            </Text>
          </Button>
          

          <Button
            style={styles.button}
            isFullWidth
            onPress={()=>{ navigation.navigate('SingUpWithEmail'); }}>
            <Text isBold isWhite>
              
Crear Cuenta
            </Text>
          </Button>

          { false &&
            <Button
            style={styles.button}
            isFullWidth
            onPress={_onConnectWithPhoneNumberButtonPressed}>
            <Text isBold isWhite>
              
Conectarse con el número de teléfono
            </Text>
          </Button>

          }
          { test &&
          <Button
            style={styles.button}
            isFullWidth
            onPress={testMode}>
            <Text isBold isWhite>Probar</Text>
          </Button>
           }
        </View>

      </Container>
    </View>
  );
};

export default Authentication;
