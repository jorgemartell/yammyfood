import * as React from 'react';
import {SafeAreaView, View, ScrollView, Alert,Picker} from 'react-native';
import {Text, TextField, Button, Dialog} from '@src/components/elements';
import useThemeColors from '@src/custom-hooks/useThemeColors';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import Api from '@src/Api'
type AuthWithPhoneNumberProps = {};

const AuthWithPhoneNumber: React.FC<AuthWithPhoneNumberProps> = () => {
  const navigation = useNavigation();
  const {card} = useThemeColors();
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [ver_password, ver_setPassword] = React.useState('');

  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const [confirm, setConfirm] = React.useState(null);
  const [code, setCode] = React.useState('');

  const [selectedValue, setSelectedValue] = React.useState("+52");


  Api.Conect()
  async function signInWithPhoneNumber() {
    
    auth().sendPasswordResetEmail(email).then(() => {
      alert('Hemos enviado un correo electrónico para restablecer su contraseña')
      navigation.goBack();
    })
    .catch(error => {
      if (error.code === 'auth/email-already-in-use') {
        alert('¡Esa dirección de correo electrónico ya está en uso!');
      }

      if (error.code === 'auth/invalid-email') {
        alert('¡Esa dirección de correo electrónico no es válida!');
      }

      console.error(error);
    });
  }
  async function confirmCode() {
    try {
      await confirm.confirm(code);
    } catch (error) {
      alert('Invalid code.');
    }
  }
  const _hideModal = () => {
    setIsModalVisible(false);
  };




  const _onNextButtonPressed = () => {
    if (email=='') {
      Alert.alert('Error', '¡Por favor, introduzca su correo!');
      return;
    }
   
    
    signInWithPhoneNumber()
  };
  const _onConfirmButtonPressed = () => {
    signInWithPhoneNumber()

    
  };

    var input = {width: '100%', height: 50,paddingTop:10,marginBottom:30}
   if (Platform.OS === 'android') {
     input = {width: '100%', height: 50,marginBottom:30}
   }
  return (
    <SafeAreaView style={styles.root}>
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.formContainer}>
          <Text isBold isHeadingTitle>
           
        Restablecimiento de contraseña
          </Text>
          <Text isSecondary hasMargin>
            enviar correo electrónico de restablecimiento de contraseña

          </Text>





          <View style={{flex: 1, flexDirection: 'row'}}>
       
        <View style={input}>
          <Text  isHeadingTitle>
            Correo Electronico
          </Text>
           <TextField
            style={[{backgroundColor: card}, styles.phoneNumberTextField]}
            value={email}
            onChangeText={(val)=>{ setEmail(val); }}
            hasMargin
            autoFocus
          />

        </View>
      </View>





         
        </View>
        <Button isFullWidth onPress={_onNextButtonPressed}>

          <Text isBold>Siguiente</Text>
        </Button>
      </ScrollView>
      <Dialog isVisible={isModalVisible} onBackdropPress={_hideModal}>
        <Text isCenter>Iniciar sesión con número de teléfono</Text>
        <Text isHeadingTitle isCenter isBold style={styles.phoneNumberText}>
          {selectedValue} 
        </Text>
        <Text isCenter>
          
Enviaremos el código de autenticación al número de teléfono que ingresó.
        </Text>
        <Text isCenter>¿Quieres continuar?</Text>
        <View style={styles.confirmButtonContainer}>
          <Button isFullWidth onPress={_onConfirmButtonPressed}>
            <Text isBold>Confirmar</Text>
          </Button>
        </View>
        <View style={styles.cancelButtonContainer}>
          <Button isFullWidth isTransparent onPress={_hideModal}>
            <Text>Cancelar</Text>
          </Button>
        </View>
      </Dialog>
    </SafeAreaView>
  );
};

export default AuthWithPhoneNumber;
