import * as React from 'react';
import {useFocusEffect} from '@react-navigation/native';
import {ScrollView, SafeAreaView, InteractionManager,Image,PermissionsAndroid} from 'react-native';
import {SearchBar, LoadingIndicator,Button,Text} from '@src/components/elements';
import PopularPlaces from './PopularPlaces';
import RecommendedPlaces from './RecommendedPlaces';
import MerchantCampaigns from './MerchantCampaigns';
import PopularCategories from './PopularCategories';
import HotDeals from './HotDeals';
import RemarkablePlaces from './RemarkablePlaces';
import AppReviewModal from '@src/components/common/AppReviewModal';
import Api from '@src/Api'
import firestore from '@react-native-firebase/firestore';
import FindResult from '@src/components/screens/FindResult';
import { SliderBox } from "react-native-image-slider-box";
import CartContext from '@src/context/cart-context';
import Geolocation from 'react-native-geolocation-service';


type HomeProps = {};

const Home: React.FC<HomeProps> = () => {
  const [
    isNavigationTransitionFinished,
    setIsNavigationTransitionFinished,
  ] = React.useState(false);
  const [SearchText, SetSearchText] = React.useState("");
  const [Banner, SetBanner] = React.useState([]);
  const [OnSearch, SetOnSearch] = React.useState(false);
  const {SetAddres,Addres} = React.useContext(CartContext);



  const requestAndroidLocationPermission = async () => {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Yammy acceso a GPS',
          message:
            'Yammy necesita acceso a su ubicación para que pueda ver dónde se encuentra en el mapa.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        _initUserLocation();
      } else {
        
      }
    };


      const _initUserLocation = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        const {longitude, latitude} = position.coords;

        Api.GetAdressFromGeo(latitude,longitude,(dir)=>{
          SetAddres(longitude,latitude)
          
        })
        
      },
      (error) => {
        console.log(error.code, error.message);
      },
      {timeout: 15000, maximumAge: 10000},
    );
  };


  useFocusEffect(
    React.useCallback(() => {
      const task = InteractionManager.runAfterInteractions(() => {


        if (Platform.OS === 'android') {
          requestAndroidLocationPermission();
        } else {
          Geolocation.requestAuthorization("whenInUse")
          _initUserLocation();
        }


        Api.List('banner',(data)=>{
          var images = []
          for (var i = 0; i < data.length; i++) {
            
            images.push("https://firebasestorage.googleapis.com/v0/b/delivery-23f4e.appspot.com/o/"+data[i].img+"?alt=media&token=f64adebe-e2b4-473a-b2c7-40c635ddf991")
          }
          SetBanner(images)
        })

        setIsNavigationTransitionFinished(true);
      });
      return () => task.cancel();
    }, []),
  );
  var S = (text)=>{


      SetSearchText(text)
      SetOnSearch(false)

      if(text==""){
        
        SetOnSearch(false)
        return
      
      }else{
        //SetOnSearch(true)
      }

      
      
      
  }




  var RenderContent=()=>{

    return (<> </>)
  }
  var renderS = ()=>{

    if(SearchText!=""){

        if(OnSearch==false){
          return (<Button onPress={()=>{ SetOnSearch(true)  }} ><Text>Buscar</Text></Button>)
        }else{
          return (<FindResult SearchText={SearchText} />)
        }
        
    }else{
      //return RenderContent()
    }
    
  }
  return (
    <SafeAreaView>
      <ScrollView stickyHeaderIndices={[0]}>

       { Addres.address =="" &&
        <><Text>Estamos esperando que active su GPS...</Text>
        <Button onPress={()=>{ if (Platform.OS === 'android') { requestAndroidLocationPermission(); } else { _initUserLocation(); }  }} >
        <Text isWhite>Reintentar</Text></Button>
        <LoadingIndicator size="large" hasMargin /></>
  }

        <SearchBar onChangeText={(text)=>{ S(text) }} placeholder="Encuentra lugares, platillos o restaurantes ..." />
        

        {renderS()}

        
        { SearchText==""&&
          <>
          <SliderBox autoplay circleLoop sliderBoxHeight={300}
  images={Banner}
  onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
  currentImageEmitter={index => console.warn(`current pos is: ${index}`)}
/>
         
          <RemarkablePlaces />
          </>
        }


      </ScrollView>
      
    </SafeAreaView>
  );
};

export default Home;
