import * as React from 'react';
import {useTheme, useNavigation,useFocusEffect} from '@react-navigation/native';
import {LoadingIndicator} from '@src/components/elements';
import {Image, View,InteractionManager} from 'react-native';
import {Text, Container, Touchable} from '@src/components/elements';
import {mockCategories} from '@src/data/mock-categories';
import styles from './styles';
import Api from '../../../../Api'
type PopularCategoriesProps = {};


const PopularCategories: React.FC<PopularCategoriesProps> = () => {

    

  const [categorias,setcategorias] = React.useState([]);
  const [load,set_load] = React.useState(true);

  useFocusEffect(
    React.useCallback(() => {
      const task = InteractionManager.runAfterInteractions(() => {
        ;
        Api.List('categorias',(data)=>{
          setcategorias(data)
          set_load(false)
        })
      });
      return () => task.cancel();
    }, []),
  );
  
  const navigation = useNavigation();
  const {
    colors: {border},
  } = useTheme();

  const _onButtonCategoryItemPressed = (name: string,id,doc) => {
    return () => {
      navigation.navigate('PlaceListScreen', {title: name,id:id,doc:doc});
    };
  };

  
  if(load){
    return (<LoadingIndicator size="large" hasMargin />)
  }
  return (
    <Container style={styles.categoryContainer}>
      {categorias.map((category) => {
        const {id, img, nombre,doc} = category;
        var img_url = "https://firebasestorage.googleapis.com/v0/b/delivery-23f4e.appspot.com/o/"+img+"?alt=media"
        return (
          <Touchable key={id} onPress={_onButtonCategoryItemPressed(nombre,id,doc)}>
            <View style={[styles.categoryItem, {borderColor: border}]}>
              <Container>
                <Image style={styles.categoryImage} source={{uri:img_url}} />
              </Container>
              <Container>
                <Text style={styles.categoryTitle}>{nombre}</Text>
              </Container>
            </View>
          </Touchable>
        );
      })}
    </Container>
  );
};

export default PopularCategories;
