import * as React from 'react';
import {useTheme, useNavigation,useFocusEffect} from '@react-navigation/native';
import {Container} from '@src/components/elements';
import {Place, mockRemarkablePlace} from '@src/data/mock-places';
import PlaceListItem from '@src/components/common/PlaceListItem';
import styles from './styles';
import Api from '@src/Api'
import {LoadingIndicator} from '@src/components/elements';
import {Image, View,InteractionManager} from 'react-native';

type FeaturedTabProps = {};

const FeaturedTab: React.FC<FeaturedTabProps> = () => {
  var data = []

  const [Empresas,setEmpresas] = React.useState([]);
  const [load,set_load] = React.useState(true);

  useFocusEffect(
    React.useCallback(() => {
      const task = InteractionManager.runAfterInteractions(() => {
        
        Api.List('empresas',(data)=>{

          var to_show = []
          for (var i = 0; i < data.length; ++i) {

            to_show.push(data[i])
            if(data[i].activeUpdate!=undefined){
              if((data[i].activeUpdate+10000) > (Date.now())){
                //to_show.push(data[i])
              }
              
            }
          }

          setEmpresas(to_show)
          set_load(false)
        })
      });
      return () => task.cancel();
    }, []),
  );
  

  return (
    <Container style={styles.tabContent}>
      {Empresas.map((doc) => {
      	var img_url = "https://firebasestorage.googleapis.com/v0/b/delivery-23f4e.appspot.com/o/"+doc.img+"?alt=media"
      
      	var item = {doc:doc,objectId:doc.objectId,title:doc.nombre,image:{uri:img_url}}
        return <PlaceListItem key={doc.objectId} doc={doc} data={item} />;
      })}
    </Container>
  );
};

export default FeaturedTab;
