import * as React from 'react';
import {SafeAreaView, View, ScrollView, Alert} from 'react-native';
import {Text, Button} from '@src/components/elements';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import useThemeColors from '@src/custom-hooks/useThemeColors';
import styles from './styles';
import {useNavigation, StackActions} from '@react-navigation/native';
import Api from '@src/Api'


type AuthVerificationCodeProps = {};

const CELL_COUNT = 6;

const AuthVerificationCode: React.FC<AuthVerificationCodeProps> = (data) => {
  

  const navigation = useNavigation();
  const {primary, border} = useThemeColors();
  const [value, setValue] = React.useState('');
  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  async function confirmCode() {
    if (value.length !== 6) {
      Alert.alert('Error', '¡El código de verificación no es válido!');
      return;
    }

    data.route.params.confirm(value)

  }
  const _onNextButtonPressed = () => {
    
    navigation.dispatch(StackActions.replace('LoginScreen'));
  };

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.formContainer}>
          <Text isBold isHeadingTitle>
            Código de verificación
          </Text>
          <Text isSecondary hasMargin>
            
Se ha enviado un código de verificación a su teléfono móvil.
          </Text>
          <View style={styles.verificationCodeContainer}>
            <CodeField
              ref={ref}
              {...props}
              value={value}
              onChangeText={setValue}
              cellCount={CELL_COUNT}
              rootStyle={styles.codeFieldRoot}
              keyboardType="number-pad"
              textContentType="oneTimeCode"
              renderCell={({index, symbol, isFocused}) => (
                <Text
                  key={index}
                  style={[
                    styles.cell,
                    {
                      borderColor: isFocused ? primary : border,
                    },
                  ]}
                  onLayout={getCellOnLayoutHandler(index)}>
                  {symbol || (isFocused ? <Cursor /> : null)}
                </Text>
              )}
            />
          </View>
        </View>
        <Button isFullWidth onPress={confirmCode}>
          <Text isBold>Siguiente</Text>
        </Button>
      </ScrollView>
    </SafeAreaView>
  );
};

export default AuthVerificationCode;
