import React , { useEffect } from 'react';
import {ScrollView, SafeAreaView,View} from 'react-native';
import DeliveryTime from './DeliveryTime';
import DeliveryStep from './DeliveryStep';
import DriverInformation from './DriverInformation';
import {Divider, Container, Button, Text,List} from '@src/components/elements';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';
import DeliveryMapView from './DeliveryMapView';
import database from '@react-native-firebase/database';
import Api from '@src/Api'
import { LoadingIndicator} from '@src/components/elements';





const Suc = ({id}) => {


  const [data, setData] = React.useState('');


   React.useEffect(() => {
      Api.Get('empresas',id,(d)=>{
        
      
        setData(d.nombre)

      })
    },[data])



  
  return (
    <View style={styles.root}>
      <Text isBold isPrimary>
                 {data}
                </Text>
    </View>
  );
};


type TrackOrderProps = {};

const TrackOrder: React.FC<TrackOrderProps> = () => {
  const navigation = useNavigation();

  const [isMapViewVisible, setIsMapViewVisible] = React.useState(false);
  const [Step, setStep] = React.useState(1);
  const [Load, setLoad] = React.useState(true);
  const [Data, setData] = React.useState(null);

  const [OrderList, setOrderList] = React.useState([]);
  const [state, setState] = React.useState({load:true,step:1,data:{}});

  const _onOrderSomethingElseButtonPressed = () => {
    navigation.navigate('HomeScreen');
  };



  
  const Item = (row)=>{
  return {
                  onPress:(it)=>{ 
                    
                    navigation.navigate('TrackOrder',{doc:row}); 
                  },
                  id:row.objectId,
                  note: row.status,
                  title: (<Suc id={row.res_id} />),
                  subTitle:`${row.items.length} articulos | $${row.total}`,
                  rightIcon: (<></>),
                }
  }

useEffect(() => {


      Api.OrderUser((data)=>{


        
        if(data.length==1){

          var row = data[0]
          row.objectId = data[0].objectId

          setOrderList([Item(row)]);
          setLoad(false)

          navigation.navigate('TrackOrder',{doc:row}); 
          


        }else{
            
            

            if(data.length>1){

              var r = []
              for (var i = 0; i < data.length; i++) {
                var row = data[i]
                r.push(Item(row))
              }

              setOrderList(r);
              setLoad(false)
            }
        }

      })


      

    },[]);
  


  if(Load){
    return (<LoadingIndicator   size="large" hasMargin/>)
  }



  


  return (
    <SafeAreaView style={styles.root}>
      <ScrollView
        contentContainerStyle={styles.scrollViewContentContainerStyle}>
        <Container>
         <SafeAreaView style={styles.root}>
           <List data={OrderList} />
         </SafeAreaView>
          
        </Container>
        
      </ScrollView>

    </SafeAreaView>
  );
};

export default TrackOrder;
