import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Account from '@src/components/screens/Account';
import List from './List'
import TrackOrder from './TrackOrder'



const Stack = createStackNavigator();

export default (props) => {
  const {navigation} = props;
 

  return (
    <Stack.Navigator initialRouteName="List">
      <Stack.Screen
        options={() => {
          return {
            title: 'Ordenes',
          };
        }}
        name="List"
        component={List}
      />
     
     <Stack.Screen
        options={() => {
          return {
            title: 'Ordenes',
          };
        }}
        name="TrackOrder"
        component={TrackOrder}
      />
      
    </Stack.Navigator>
  );
};

