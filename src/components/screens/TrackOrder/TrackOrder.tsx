import React , { useEffect } from 'react';
import {ScrollView, SafeAreaView,View} from 'react-native';
import DeliveryTime from './DeliveryTime';
import DeliveryStep from './DeliveryStep';
import DriverInformation from './DriverInformation';
import {Divider, Container, Button, Text,List} from '@src/components/elements';
import {useNavigation} from '@react-navigation/native';
import styles from './styles';
import DeliveryMapView from './DeliveryMapView';
import database from '@react-native-firebase/database';
import Api from '@src/Api'
import { LoadingIndicator} from '@src/components/elements';
import Parse from "parse/react-native.js";




const Suc = ({id}) => {


  const [data, setData] = React.useState('');


   React.useEffect(() => {
      
      Api.Get('empresas',id,(d)=>{
        
       
        setData(d.nombre)

      })
    },[data])



  
  return (
    <View style={styles.root}>
      <Text isBold isPrimary>
                 {data}
                </Text>
    </View>
  );
};


type TrackOrderProps = {};

const TrackOrder: React.FC<TrackOrderProps> = (data) => {
  const navigation = useNavigation();

  const [isMapViewVisible, setIsMapViewVisible] = React.useState(false);
  const [Step, setStep] = React.useState(0);
  const [Load, setLoad] = React.useState(true);
  const [Data, setData] = React.useState(null);

  const [OrderList, setOrderList] = React.useState([]);

  const [state, setState] = React.useState({load:true,step:0,data:{}});

  const _onOrderSomethingElseButtonPressed = () => {
    navigation.navigate('HomeScreen');
  };

  const _onMapViewButtonPressed = () => {
    setIsMapViewVisible(!isMapViewVisible);
  };

    


var LoadStep = (res)=>{


            if(res.get('status')=='nuevo'){
              setStep(2)
              setState({data:data.route.params.doc,load:false,step:1})
            }

            if(res.get('status')=='preparando'){
              setStep(3)
              setState({data:data.route.params.doc,load:false,step:2})
            }
            
            if(res.get('status')=='para entrega'){
              setStep(3)
              setState({data:data.route.params.doc,load:false,step:3})
            }

            //if(snapshot.val().status=='en_camino'){
            //  setState({data:data.docs[0].data(),load:false,step:4})
            //}
            
            if(res.get('status')=='entregado'){
              setStep(4)
              navigation.navigate('HomeScreen');
              Api.ShowEntrega(false)
              setState({data:data.route.params.doc,load:false,step:4})
              
            }
}
useEffect(async() => {



        

          let query = new Parse.Query('ordenes');

        query.equalTo("objectId", data.route.params.doc.objectId);
        let subscription = await query.subscribe();
        
        subscription.on('update', (object) => {
          LoadStep(object)
        })

        const results = await query.find();
        LoadStep(results[0])

    },[]);
  

  

  if(state.load){
    return (<LoadingIndicator   size="large" hasMargin/>)
  }



  


  return (
    <SafeAreaView style={styles.root}>
      <ScrollView
        contentContainerStyle={styles.scrollViewContentContainerStyle}>
        <Container>
          { false && <DeliveryTime />}
          <Divider />
          
        </Container>
        {isMapViewVisible ? <DeliveryMapView data={data.route.params.doc} step={state.step} /> : <DeliveryStep step={state.step} />}
      </ScrollView>

      <Container style={styles.footerButtonContainer}>
        
        { false && <Button isFullWidth onPress={_onMapViewButtonPressed}>
          <Text isWhite isBold style={styles.mapViewText}>
            {!isMapViewVisible ? 'Vista del mapa' : 'Vista de estado del pedido'}
          </Text>
        </Button>
        }
        <Button
          isFullWidth
          isTransparent
          onPress={_onOrderSomethingElseButtonPressed}
          style={styles.cancelOrderButton}>
          
        </Button>
      </Container>
    </SafeAreaView>
  );
};

export default TrackOrder;
