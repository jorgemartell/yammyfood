import React , { useEffect } from 'react';
import {View, Image} from 'react-native';
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  MapEvent,
  Region,
  Polyline,
} from 'react-native-maps';
import styles from './styles';
import {darkModeStyle} from '@src/utils/googleMapStyle';
import {useTheme} from '@react-navigation/native';
import database from '@react-native-firebase/database';
import { LoadingIndicator} from '@src/components/elements';
import MapViewDirections from 'react-native-maps-directions';
import Api from '@src/Api'

type DeliveryMapViewProps = {data:{},step};

const DeliveryMapView: React.FC<DeliveryMapViewProps> = (data) => {
  const {
    colors: {primary},
  } = useTheme();

  const [currentLocation, setCurrentLocation] = React.useState<Region>({
    longitude: data.data.entrega.longitude,
    latitude: data.data.entrega.latitude,
    longitudeDelta: 0.0022,
    latitudeDelta: 0.0031,
  });


  const [loc_suc, Setloc_suc] = React.useState<Region>({
    longitude: 0,
    latitude: 0,
    longitudeDelta: 0.0022,
    latitudeDelta: 0.0031,
  });
  const [Load, setLoad] = React.useState(true);
  const [gps_entrega, set_gps_entrega] = React.useState(null);
  
  var GOOGLE_MAPS_APIKEY = "AIzaSyBO_bgV08VhRvjAx6yf2pH_9Yk71L3c_v8"

  const _onRegionChangeComplete = (region: Region) => {
    setCurrentLocation(region);
  };
  useEffect(() => {


    Api.Empresa(data.data.res_id,(empre_data)=>{
      
      Setloc_suc({longitude:empre_data.data().longitude,latitude:empre_data.data().latitude})
      setLoad(false)
      

    })


    database().ref('/repartidores/'+data.data.repartidor).on('value', snapshot => {
        if(snapshot.val()!=null){
          set_gps_entrega(snapshot.val())
          
        }
      })
    
  },[data.step])
  const _onMapViewPressed = (event: MapEvent) => {
    const {
      nativeEvent: {coordinate},
    } = event;

  
  };

  if(Load){
    return (<LoadingIndicator />)
  }

  var desde ={}
  if( (data.step==1) || (data.step==2) || (data.step==3)){
    desde = {
            longitude: parseFloat(loc_suc.longitude),
            latitude:  parseFloat(loc_suc.latitude),
           
          }

  }else{
    if(gps_entrega==null){
    return (<LoadingIndicator />)
    }
    desde=  {
            longitude: gps_entrega.longitude,
            latitude: gps_entrega.latitude,
        
          }
  }
   
  return (
    <View style={styles.mapViewContainer}>
      <MapView
        loadingEnabled
        cacheEnabled
        loadingIndicatorColor="black"
        loadingBackgroundColor="black"
        provider={PROVIDER_GOOGLE}
        style={styles.mapView}
        customMapStyle={darkModeStyle}
        onRegionChangeComplete={_onRegionChangeComplete}
        onPress={_onMapViewPressed}
        initialRegion={currentLocation}>
        <Marker
          coordinate={desde}>
          <View style={styles.currentLocationMarkerContainer}>
            <Image
              source={require('../../../assets/drivers/location.png')}
              style={styles.marker}
            />
          </View>
        </Marker>

        <Marker
          coordinate={{
            ...currentLocation,
            longitude: data.data.entrega.longitude,
            latitude: data.data.entrega.latitude,
          }}>
          
        </Marker>
        

        <MapViewDirections
    origin={{longitude:desde.longitude,latitude:desde.latitude}}
    destination={{
            longitude: data.data.entrega.longitude,
            latitude: data.data.entrega.latitude,
          }}
    apikey={GOOGLE_MAPS_APIKEY}
    strokeWidth={3}
    strokeColor="hotpink"
  />


      </MapView>
    </View>
  );
};

export default DeliveryMapView;
