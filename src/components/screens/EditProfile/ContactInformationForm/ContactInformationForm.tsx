import * as React from 'react';
import {Container, TextField, Text, Button} from '@src/components/elements';
import {Profile} from '@src/data/mock-profile';
import styles from './styles';
import Api from '@src/Api'

type ContactInformationFormProps = {
  profile: Profile;
};

const ContactInformationForm: React.FC<ContactInformationFormProps> = ({
  profile,
}) => {



  return (
    <Container style={styles.container}>
      <TextField defaultValue={Api.UserData.nombre} onChangeText={(val)=>{ Api.UserData.nombre = val }} textContentType="name" hasMargin />
     
      
      <Text isSecondary style={styles.note}>
       
      </Text>
      <Button onPress={()=>{ Api.SaveUser(Api.UserData,()=>{ alert('Datos guardados...')  }) }} >

        <Text isWhite isBold>
          Guardar
        </Text>
      </Button>
    </Container>
  );
};
export default ContactInformationForm;
