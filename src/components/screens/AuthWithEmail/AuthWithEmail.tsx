import * as React from 'react';
import {SafeAreaView, View, ScrollView, Alert,Picker} from 'react-native';
import {Text, TextField, Button, Dialog} from '@src/components/elements';
import useThemeColors from '@src/custom-hooks/useThemeColors';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';
import auth from '@react-native-firebase/auth';
import Api from '@src/Api'




type AuthWithPhoneNumberProps = {};

const AuthWithPhoneNumber: React.FC<AuthWithPhoneNumberProps> = () => {
  const navigation = useNavigation();
  const {card} = useThemeColors();
  const [email, setEmail] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [isModalVisible, setIsModalVisible] = React.useState(false);
  const [confirm, setConfirm] = React.useState(null);
  const [code, setCode] = React.useState('');

  const [selectedValue, setSelectedValue] = React.useState("+52");


  Api.Conect()
  async function signInWithPhoneNumber() {
    
    auth().signInWithEmailAndPassword(email, password).then((data) => {
      navigation.goBack()
    })
    .catch(error => {
      if (error.code === 'auth/wrong-password') {
        alert('La contraseña no es válida o el usuario no tiene contraseña.');
      }

      if (error.code === 'auth/invalid-email') {
        alert('¡Esa dirección de correo electrónico no es válida!');
      }

      console.error(error);
    });
  }
  async function confirmCode() {
    try {
      await confirm.confirm(code);
    } catch (error) {
      alert('Invalid code.');
    }
  }
  const _hideModal = () => {
    setIsModalVisible(false);
  };








  const _onNextButtonPressed = () => {
    if (email=='') {
      Alert.alert('Error', '¡Por favor, introduzca su número de teléfono!');
      return;
    }
    
    if (password=='') {
      Alert.alert('Error', '¡Por favor, introduzca su número de teléfono!');
      return;
    }
    signInWithPhoneNumber()
  };
  const _onConfirmButtonPressed = () => {
    signInWithPhoneNumber()

    
  };

    var input = {width: '100%', height: 50,paddingTop:10,marginBottom:30}
   if (Platform.OS === 'android') {
     input = {width:'100%', height: 50,marginBottom:30}
   }
  return (
    <SafeAreaView style={styles.root}>
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.formContainer}>
          <Text isBold isHeadingTitle>
           
        Ingrese su Correo electrónico
          </Text>
          <Text isSecondary hasMargin>
            
          </Text>





          <View style={{flex: 1, flexDirection: 'row'}}>
       
        <View style={input}>
          <Text isBold isHeadingTitle>
            Correo Electronico
          </Text>
           <TextField
            style={[{backgroundColor: card}, styles.phoneNumberTextField]}
            value={email}
            onChangeText={(val)=>{ setEmail(val); }}
            hasMargin
            autoFocus
          />

        </View>
      </View>


        <View style={{flex: 1, flexDirection: 'row'}}>
       
        <View style={input}>
          
          <Text isBold isHeadingTitle>
            Contraseña
          </Text>
           <TextField
            style={[{backgroundColor: card}, styles.phoneNumberTextField]}
            value={password}
            onChangeText={(val)=>{ setPassword(val) }}
            secureTextEntry={true}
            hasMargin
            autoFocus
          />

        </View>
      </View>


          

         
        </View>
        <Button isFullWidth onPress={_onNextButtonPressed}>

          <Text isBold>Iniciar</Text>
        </Button>

        <Text></Text>
        <Button isFullWidth onPress={()=>{ navigation.navigate('SendPasswordResetEmail'); }}>

          <Text isBold>Restablecimiento de contraseña</Text>
        </Button>
      </ScrollView>
      <Dialog isVisible={isModalVisible} onBackdropPress={_hideModal}>
        <Text isCenter>Iniciar sesión con número de teléfono</Text>
        <Text isHeadingTitle isCenter isBold style={styles.phoneNumberText}>
          {selectedValue} 
        </Text>
        <Text isCenter>
          
Enviaremos el código de autenticación al número de teléfono que ingresó.
        </Text>
        <Text isCenter>¿Quieres continuar?</Text>
        <View style={styles.confirmButtonContainer}>
          <Button isFullWidth onPress={_onConfirmButtonPressed}>
            <Text isBold>Confirmar</Text>
          </Button>
        </View>
        <View style={styles.cancelButtonContainer}>
          <Button isFullWidth isTransparent onPress={_hideModal}>
            <Text>Cancelar</Text>
          </Button>
        </View>
      </Dialog>
    </SafeAreaView>
  );
};

export default AuthWithPhoneNumber;
