import * as React from 'react';
import {I18nManager, ScrollView, View} from 'react-native';
import {
  Section,
  Icon,
  Container,
  Text,
  Button,
  Divider,
} from '@src/components/elements';
import ListRowItem from '@src/components/elements/List/ListRowItem';
import LottieView from 'lottie-react-native';
import styles from './styles';
import {useNavigation} from '@react-navigation/native';


type SupportCenterPros = {};

const SupportCenter: React.FC<SupportCenterPros> = () => {
  const chevronIconName = I18nManager.isRTL ? 'chevron-left' : 'chevron-right';
  const navigation = useNavigation();

  return (
    <ScrollView>
      <Container style={styles.contactsupportCenterContainer}>
        <View style={styles.supportCenterLeft}>
          <Text isHeadingTitle isPrimary isBold>
            Chat en vivo con nuestro equipo de soporte
          </Text>
          <Button onPress={()=>navigation.navigate('SupportCenterChatScreen')} style={styles.supportCenterButton}>
            <Text isWhite isBold>
              Comienzo
            </Text>
          </Button>
        </View>
        <View style={styles.supportCenterRight}>
          <LottieView
            source={require('@src/assets/animations/support-center.json')}
            autoPlay
            style={styles.supportCenterLottieView}
          />
        </View>
      </Container>
      
    </ScrollView>
  );
};

export default SupportCenter;
