import * as React from 'react';
import {View, Image, Platform,PermissionsAndroid} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {AndroidEvent} from '@react-native-community/datetimepicker';
import {
  Container,
  Text,
  Button,
  Section,
  Divider,
  DateTimePicker,
} from '@src/components/elements';
import styles from './styles';
import Geolocation from 'react-native-geolocation-service';
import Api from '@src/Api'
import CartContext from '@src/context/cart-context';

type DeliveryInformationProps = {};

const DeliveryInformation: React.FC<DeliveryInformationProps> = () => {
  const navigation = useNavigation();
  const [date, setDate] = React.useState(new Date(1598051730000));
  const [showDateTimePicker, setShowDateTimePicker] = React.useState(false);
  const {driverNotes,SetAddres,Addres} = React.useContext(CartContext);

  const onChangeAddressButtonPressed = () => {
    navigation.navigate('ChangeAddressScreen',{change:(atitude, longitude,dir)=>{

      
    }});
  };

  const onChange = (event: AndroidEvent, selectedDate?: Date) => {
    const currentDate = selectedDate || date;
    setShowDateTimePicker(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const _onChangeTimeButtonPressed = () => {
    setShowDateTimePicker(!showDateTimePicker);
  };

  const _initUserLocation = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        const {longitude, latitude} = position.coords;

        SetAddres(longitude,latitude)
        
        
      },
      (error) => {
        console.log(error.code, error.message);
      },
      {timeout: 15000, maximumAge: 10000},
    );
  };

  React.useEffect(() => {
    
    const requestAndroidLocationPermission = async () => {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Food Delivery App Permission',
          message:
            'Food Delivery App needs access to your location ' +
            'so you see where you are on the map.',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        _initUserLocation();
      } else {
        console.log('Camera permission denied');
      }
    };

    if (Platform.OS === 'android') {
      requestAndroidLocationPermission();
    } else {
      _initUserLocation();
    }
  }, []);
  return (
    <Section
      title="Entregar a"
      actionButtonText="Cambiar dirección"
      onButtonActionPressed={onChangeAddressButtonPressed}>
      <Container>
        <View style={styles.deliveryContainer}>
          { false &&
          <View style={styles.locationContainer}>
            <Image
              source={require('@src/assets/checkout/map.png')}
              style={styles.locationImage}
            />
          </View>
          }
          <View>
            <Text isBold style={styles.locationInfo}>
              {Addres.address}
            </Text>
            { false &&
            <Text
              isSecondary
              accessibilityRole="link"
              style={styles.locationInfo}>
              Add floor / unit number
            </Text>
          }
           <Text onPress={()=>{ navigation.navigate('DriverNotas'); }} isSecondary style={styles.locationInfo}>
             { driverNotes =="" &&
              <>Agregar una nota al conductor</>
             }
             { driverNotes != "" &&

               <>{driverNotes}</>
             }
            </Text>
           
          </View>
        </View>
        
        
      </Container>
      {showDateTimePicker && (
        <DateTimePicker
          value={date}
          mode="datetime"
          onChange={onChange}
          style={styles.dateTimePicker}
        />
      )}
    </Section>
  );
};

export default DeliveryInformation;
