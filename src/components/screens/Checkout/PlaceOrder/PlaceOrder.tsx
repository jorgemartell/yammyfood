import * as React from 'react';
import {View} from 'react-native';
import {Container, Text, Button} from '@src/components/elements';
import SuccessOrderModal from './SuccessOrderModal';
import styles from './styles';
import {formatCurrency} from '@src/utils/number-formatter';
import Api from '@src/Api'
import AuthContext from '@src/context/auth-context';
import {useNavigation,StackActions} from '@react-navigation/native';

type PlaceOrderProps = {
  totalPrice: number;
  shippingFee: number;
};

const PlaceOrder: React.FC<PlaceOrderProps> = ({cartItems,totalPrice, shippingFee,restaurante,pago,enSucursal}) => {
  const [
    isSuccessOrderModalVisible,
    setIsSuccessOrderModalVisible,
  ] = React.useState(false);
  const {userToken} = React.useContext(AuthContext);
  const navigation = useNavigation();
  const _onPlaceOrderButtonPressed = () => {
    if(cartItems.length==0){
      alert('No hay articulos')
      return 
    }

    if(Api.Pago==null){
      alert('indique un metodo de pago')
    }else{
      setIsSuccessOrderModalVisible(true);
    }
  };

  var sub_total = 0;
  for (var i = 0; i < cartItems.length; i++) {
    sub_total = sub_total+cartItems[i].subtotal;
  }
  
  return (
    <Container style={styles.placeOrderContainer}>
      <View style={styles.totalPriceContainer}>
        <Text style={styles.totalPriceText}>Total</Text>
        <Text isBold style={styles.totalPriceText}>
          {formatCurrency(sub_total + shippingFee)}
        </Text>
      </View>

      { userToken=='' &&
      <Button isFullWidth onPress={()=>{ navigation.navigate('Auth',{login:()=>{ _onPlaceOrderButtonPressed()}})  }}>
        <Text isBold style={styles.placeOrderText}>
         Iniciar sesion y realizar pedido
        </Text>
      </Button>
      }

      { userToken!='' &&
      <Button isFullWidth onPress={_onPlaceOrderButtonPressed}>
        <Text isBold style={styles.placeOrderText}>
          Realizar pedido
        </Text>
      </Button>
      }
    

      <SuccessOrderModal

        restaurante={restaurante}
        enSucursal={enSucursal}
        isVisible={isSuccessOrderModalVisible}
        setIsVisble={setIsSuccessOrderModalVisible}
        envio={shippingFee}
        pago={pago}
      />
    </Container>
  );
};

export default PlaceOrder;
