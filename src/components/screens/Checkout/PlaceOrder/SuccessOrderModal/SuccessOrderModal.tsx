import * as React from 'react';
import {View, Animated} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {StackActions} from '@react-navigation/native';
import LottieView from 'lottie-react-native';
import {Container, Text, Button, Dialog} from '@src/components/elements';
import CartContext from '@src/context/cart-context';
import styles from './styles';
import Api from '@src/Api'
import database from '@react-native-firebase/database';
import { AsyncStorage } from 'react-native';


type OrderSuccessModalProps = {
  isVisible: boolean;
  setIsVisble: (value: React.SetStateAction<boolean>) => void;
};

const OrderSuccessModal: React.FC<OrderSuccessModalProps> = ({
  isVisible,
  setIsVisble,
  restaurante,
  envio,
  enSucursal,
  pago
}) => {
  const navigation = useNavigation();
  const fadeIn = React.useRef(new Animated.Value(0)).current;
  const fadeOut = React.useRef(new Animated.Value(1)).current;
  const [isAnimationFinished, setIsAnimationFinished] = React.useState(false);


  const {cartItems, totalPrice,driverNotes,Addres} = React.useContext(CartContext);
  
  React.useEffect(() => {
    Animated.timing(fadeIn, {
      toValue: isAnimationFinished ? 1 : 0,
      duration: 200,
      useNativeDriver: true,
    }).start();
    Animated.timing(fadeOut, {
      toValue: isAnimationFinished ? 0 : 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
  }, [isAnimationFinished, fadeIn, fadeOut]);
  const {clearCart} = React.useContext(CartContext);

  const _onAnimationFinish = () => {

 Api.GetUserId((UserId)=>{



  var restaurantes_order = {}

  for (var i = 0; i < cartItems.length; i++) {
    var item = cartItems[i];

    var s_data = {
      items:[],
      folio:Date.now()+'-'+cartItems.length,
      total:0,
      pago:{"__type": "Pointer","className": "metodopago",objectId:Api.Pago.value},
      entrega:Addres,
      status:'nuevo',
      enSucursal:enSucursal,
      active:true,
      restaurante:{"__type": "Pointer","className": "empresas",objectId:item.res},
      usuario_location:{
          "__type": "GeoPoint",
          "latitude": Addres.latitude,
          "longitude": Addres.longitude
        },
      usuario:{"__type": "Pointer","className": "users",objectId:UserId},
      costo_envio:envio

    }


    restaurantes_order[item.res] = s_data

  }
  for (var i = 0; i < cartItems.length; i++) {
    var item = cartItems[i];
    var to_save = {
      title:item.dish.title,
      prod:item.dish.objectId,
      res_id:item.res,
      qty:item.qty,
      priceunit:item.priceunit,
      subtotal:item.subtotal,
      sub_p:item.sub_p,
      articulo:{"__type": "Pointer","className": "productos",objectId:item.dish.objectId}

    }

    restaurantes_order[item.res].items.push(to_save)
  }

 
 
    Object.keys(restaurantes_order).forEach(function(key) {
        
        var s_data = restaurantes_order[key]
        var sub_total = 0;
        for (var i = 0; i < s_data.items.length; i++) {
          sub_total = sub_total+s_data.items[i].subtotal;
        }
        s_data.total = sub_total;
        s_data.driverNotes = driverNotes;
        /*Api.Get('empresas',s_data.res_id,(empresa)=>{

            Api.GetUser((user_data)=>{
              to_ws(s_data,user_data,empresa)
        
            })
        })*/
        

        Api.Save('ordenes',s_data,(data)=>{
        
          Api.OrderId = data.objectId
          Api.OrderData = s_data;

           setIsAnimationFinished(true);
           clearCart(restaurante.objectId);



          
        })

    })

  })
  
    
  };

  const _onBackdropPress = () => {

  

    navigation.goBack();
    setIsVisble(false);
    setIsAnimationFinished(false);

  };

  const _onOrderSomethingElseButtonPressed = () => {
    clearCart(restaurante.objectId);
    setIsVisble(false);
    navigation.navigate('HomeScreen');
  };

  const _onTrackOrderButtonPressed = () => {
    clearCart(restaurante.objectId);
    setIsVisble(false);
     navigation.goBack();
     Api.ToEngtrga()

    //navigation.dispatch(StackActions.replace('TrackOrderScreen'));
  };

  return (
    <Dialog isVisible={isVisible} onBackdropPress={_onBackdropPress}>
      <Container style={styles.container}>
        <View style={styles.content}>
          <LottieView
            source={require('@src/assets/animations/order-success.json')}
            autoPlay
            loop={false}
            onAnimationFinish={_onAnimationFinish}
            style={styles.lottieView}
          />
          {!isAnimationFinished && (
            <Animated.View
              style={[styles.processingOrderContainer, {opacity: fadeOut}]}>
              <Text isBold>Procesando su pedido ...</Text>
            </Animated.View>
          )}
          <Animated.View
            style={[styles.successMessageContainer, {opacity: fadeIn}]}>
            <Text isHeadingTitle isBold isPrimary>
              Gracias por su orden.
            </Text>
            <Text isCenter style={styles.successMessage}>
              Puede realizar un seguimiento de la entrega en la sección "Pedidos".
            </Text>
          </Animated.View>
        </View>
        <Animated.View
          style={[styles.footerButtonContainer, {opacity: fadeIn}]}>
          <Button isFullWidth onPress={_onTrackOrderButtonPressed}>
            <Text isWhite isBold>
              Seguimiento de mi pedido
            </Text>
          </Button>
          <Button
            isFullWidth
            isTransparent
            style={styles.orderSomethingButton}
            onPress={_onOrderSomethingElseButtonPressed}>
            <Text>Pide algo más</Text>
          </Button>
        </Animated.View>
      </Container>
    </Dialog>
  );
};
export default OrderSuccessModal;
