import * as React from 'react';
import {ScrollView, View,Switch,Text} from 'react-native';
import DeliveryInformation from './DeliveryInformation';
import OrderSummary from './OrderSummary';
import PaymentMethod from './PaymentMethod';
import styles from './styles';
import PlaceOrder from './PlaceOrder';
import DishesAlsoOrdered from './DishesAlsoOrdered';
import CartContext from '@src/context/cart-context';
import Api from '@src/Api'

type BasketProps = {};

const shippingFee = 5;

const Basket: React.FC<BasketProps> = (data) => {

 
  const {cartItems, totalPrice,removeCartItem,SetAddres,Addres} = React.useContext(CartContext);
  var items = []
  for (var i = 0; i < cartItems.length; ++i) {
     if(data.route.params.restaurante.objectId==cartItems[i].res){
       items.push(cartItems[i])
     }
  }
  const [pago,set_pago] = React.useState({label:''});
  const [en_sucursal,set_en_sucursal] = React.useState(false);
  
  var  k = Api.getKilometros(data.route.params.restaurante.latitude,data.route.params.restaurante.longitude,Addres.latitude,Addres.longitude)
  var costo_envio = (k*12)+35

  if(en_sucursal){
    costo_envio = 0
  }
  return (
    <View style={styles.rootContainer}>
      <ScrollView
        contentInset={{
          bottom: 25,
        }}>
        { en_sucursal == false &&
          <DeliveryInformation />
        }
        <View style={{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }}>
  <Text >Recojer en sucursal</Text>
        <Switch
        trackColor={{ false: "#767577", true: "#81b0ff" }}
        thumbColor={en_sucursal ? "#FB8C00" : "#f4f3f4"}
        ios_backgroundColor="#3e3e3e"
        onValueChange={set_en_sucursal}
        value={en_sucursal}
      />
      </View>
        <OrderSummary
          restaurante={data.route.params.restaurante}
          onEnvio={(val)=>{ set_envio(val) }}
          removeCartItem={removeCartItem}
          cartItems={items}
          totalPrice={totalPrice}
          shippingFee={costo_envio}
          km={k}
        />
        
        <PaymentMethod addpayment={(p)=>{ set_pago(p) }} />
      </ScrollView>
      <PlaceOrder cartItems={items} restaurante={data.route.params.restaurante} enSucursal={en_sucursal} pago={pago} totalPrice={totalPrice} shippingFee={costo_envio} />
    </View>
  );
};

export default Basket;
