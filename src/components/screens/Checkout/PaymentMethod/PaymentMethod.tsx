import * as React from 'react';
import {Section, Container, Text, Icon, Button} from '@src/components/elements';
import styles from './styles';
import {View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import CartContext,{Radio} from '@src/context/cart-context';
import Api from '@src/Api';



type PaymentMethodProps = {};

const PaymentMethod: React.FC<PaymentMethodProps> = ({onSelectPayment}) => {
  const navigation = useNavigation();
  const [pago,set_pago] = React.useState({label:''});
  const _onAddAPromoButtonPressed = () => {
    navigation.navigate('PromotionScreen');
  };

  const _onPaymentMethodButtonPressed = () => {
    navigation.navigate('PaymentMethodScreen',{addpayment:(data)=>{
      
      set_pago(data)
      Api.Pago = data;
    }});
  };
  
  return (


      <Section title="Método de pago">
      <Container style={styles.paymentMethodContainer}>
        <View style={styles.paymentSelection}>
          <Button
            icon={<Icon name="money-check-alt" size={16} />}
            isTransparent
            onPress={_onPaymentMethodButtonPressed}>
            <Text style={styles.paymentMethodText}> {pago.label}</Text>
          </Button>
        </View>
        <Button isTransparent onPress={_onPaymentMethodButtonPressed}>
          <Text isPrimary>Modificar</Text>
        </Button>
      </Container>
    </Section>

 
  );
};

export default PaymentMethod;
