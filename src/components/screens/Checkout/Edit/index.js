import * as React from 'react';
import {Section, Container, Text, Icon, Button} from '@src/components/elements';
import {View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import CartContext,{Radio} from '@src/context/cart-context';
import Api from '@src/Api';



const PaymentMethod  = ({route}) => {
  const navigation = useNavigation();
  const [pago,set_pago] = React.useState({label:''});
  const {removeCartItem} = React.useContext(CartContext);

   
 
  return (


      <>
      <Button isTransparent onPress={()=>{ 
        removeCartItem(route.params.cartItem)
       }}>
          <Text isPrimary>Quitar Articulo</Text>
        </Button>
      </>

 
  );
};

export default PaymentMethod;