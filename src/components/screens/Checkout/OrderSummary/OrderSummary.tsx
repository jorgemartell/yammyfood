import * as React from 'react';
import {View,TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Container, Text, Section, Divider,Button} from '@src/components/elements';
import styles from './styles';
import {CartItem} from '@src/context/cart-context';
import {formatCurrency} from '@src/utils/number-formatter';
import Api from '@src/Api'
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import CartContext,{Radio} from '@src/context/cart-context';

type OrderSummaryProps = {
  cartItems: CartItem[];
  totalPrice: number;
  shippingFee: number;
};


const SubProduct = ({id})=>{

  const [text, setText] = React.useState("...");
  React.useEffect(() => {
    
    Api.Get('subproductos',id,(data)=>{
      setText(data.nombre)
    })
  },[])

  return (<Text isSecondary style={styles.sideDishText}>
                    {text}
                  </Text>)
}
const Articulo = ({cartItem,cartItemIndex,removeCartItem})=>{

const navigation = useNavigation();
const [backgroundColor, setbackgroundColor] = React.useState("");
const [Delete, setDelete] = React.useState(false);

var sub = ()=>{

  if(cartItem.sub_p==undefined){return;}
  var r = []
  for (var i = 0; i < cartItem.sub_p.length; i++) {
    var s_id = cartItem.sub_p[i]
    r.push(<SubProduct id={s_id} />)
  }
  return r
}
var onSwipeRight = (gestureState)=> {
    //this.setState({myText: 'You swiped right!'});

  }

 var onSwipe = (gestureName, gestureState)=> {
    const {SWIPE_UP, SWIPE_DOWN, SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
    
    switch (gestureName) {

      case SWIPE_LEFT:
        setDelete(true)
        break;

      case SWIPE_RIGHT:
      setDelete(false)
        break;
    }
  }

  const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };
return (<GestureRecognizer 
  onSwipe={(direction, state) => onSwipe(direction, state)}
  onSwipeRight={(state) => onSwipeRight(state)} 
  config={config}
 
  >



        <View style={styles.menuContainer}>


          <View style={styles.menuInfo}>

            

            
            <Text style={styles.quantityText}>
            
            
            {cartItem.qty}</Text>
             
              <View key={cartItemIndex}>

                <Text style={styles.mainDishText} isBold>
                  {cartItem.dish.title}
                </Text>
                {cartItem.sideDishes.map((dish, dishIndex) => (
                  <Text isSecondary key={dishIndex} style={styles.sideDishText}>
                    {dish.title}
                  </Text>
                ))}
                {sub()}
              </View>
              
          </View>


           { Delete &&
                    
                <Text onPress={()=>{ removeCartItem(cartItem); }} isBold isPrimary>
                  Remover
                </Text>
               
              
            }
          { Delete==false &&
          <Text isBold>{formatCurrency(cartItem.subtotal)}</Text>
          }
        </View>
        <Divider /></GestureRecognizer>)

}



const OrderSummary: React.FC<OrderSummaryProps> = ({
  cartItems,
  totalPrice,
  shippingFee,
  restaurante,
  km,
  removeCartItem
}) => {
  const navigation = useNavigation();

  const _onAddItemButtonPressed = () => {
    navigation.navigate('DishDetailsModal');
  };

  const [text, setText] = React.useState("...");
  React.useEffect(() => {
    
     if(cartItems.length==0){
       //navigation.goBack()

     }
  },[cartItems.length])



  var sub_total = 0;
  for (var i = 0; i < cartItems.length; i++) {
    sub_total = sub_total+cartItems[i].subtotal;
  }
  return (
    <Section
      title="Resumen del pedido"
      
      onButtonActionPressed={_onAddItemButtonPressed}>
      <Container>
         {cartItems.map((cartItem, cartItemIndex) => (
           <Articulo cartItem={cartItem} removeCartItem={removeCartItem} cartItemIndex={cartItemIndex} />


        ))}


        
        <View style={styles.priceContainer}>
          <View style={styles.subTotalContainer}>
            <Text>Sub-Total</Text>
            <Text>{formatCurrency(sub_total)}</Text>
          </View>
          <View style={styles.deliveryFee}>
            <Text>Entrega</Text>
            <Text>{formatCurrency(shippingFee)}</Text>
          </View>
        </View>
      </Container>
    </Section>
  );
};

export default OrderSummary;
