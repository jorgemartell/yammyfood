import * as React from 'react';
import {SafeAreaView, View, ScrollView, Alert, Image} from 'react-native';
import {Text, TextField, Button} from '@src/components/elements';
import useThemeColors from '@src/custom-hooks/useThemeColors';
import styles from './styles';
import AuthContext from '@src/context/auth-context';
import {useNavigation} from '@react-navigation/native';
import Api from '@src/Api'
type LoginProps = {};

const Login: React.FC<LoginProps> = () => {
  const navigation = useNavigation();
  const {signIn} = React.useContext(AuthContext);
  const {card} = useThemeColors();
  const [password, setPassword] = React.useState('');
  const [phone, setPhone] = React.useState('');

  const _onPasswordFieldChange = (value: string) => {
    setPassword(value);
  };

  const _onNextButtonPressed = () => {


    if (password=='') {
      Alert.alert('Error', 'Por favor, escriba su nombre!');
      return;
    }
    if (phone=='') {
      Alert.alert('Error', 'Por favor, escriba su telefono!');
      return;
    }
    Api.SaveUser({nombre:password,phone:phone},()=>{
      signIn();
    })
    
    
  };
  const _onForgotPasswordButtonPressed = () => {
    navigation.navigate('ForgotPasswordScreen');
  };

  return (
    <SafeAreaView style={styles.root}>
      <ScrollView contentContainerStyle={styles.contentContainer}>
        <View style={styles.formContainer}>
        
          <Text isBold isHeadingTitle>
            ¡Bienvenido!
          </Text>
          <Text  hasMargin></Text>
          <Text  hasMargin>
            Por favor, escriba su nombre
          </Text>
          <TextField
            autoFocus
            style={[{backgroundColor: card}, styles.passwordTextField]}
            value={password}
            onChangeText={_onPasswordFieldChange}
            hasMargin
           
          /><Text  hasMargin></Text>
          <Text  hasMargin>
            y escriba su telefono
          </Text>
          <TextField
            autoFocus
            style={[{backgroundColor: card}, styles.passwordTextField]}
            value={phone}
            onChangeText={(val)=>{ setPhone(val); }}
            keyboardType="phone-pad"
            hasMargin
           
          />
        </View>
        <Button isFullWidth onPress={_onNextButtonPressed}>
          <Text isBold>Continuar</Text>
        </Button>

      </ScrollView>
    </SafeAreaView>
  );
};

export default Login;
