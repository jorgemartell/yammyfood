import * as React from 'react';
import {View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {Rating, Button, Icon, Text} from '@src/components/elements';
import {Place} from '@src/data/mock-places';
import styles from './styles';
import Api from '@src/Api'
import CartContext from '@src/context/cart-context';


type PlaceCardInfoProps = {
  data: Place;
  ratingStarBackgroundColor?: string;
  doc:{}
};

const PlaceCardInfo: React.FC<PlaceCardInfoProps> = ({
  data,
  ratingStarBackgroundColor,
  doc
}) => {
  const {distance, rating, time,latitude,longitude} = data;
  const {SetAddres,Addres} = React.useContext(CartContext);

  var getKilometros = (lat1,lon1,lat2,lon2)=>
 {
 var rad = (x)=>{return x*Math.PI/180;}
var R = 6378.137; //Radio de la tierra en km
 var dLat = rad( lat2 - lat1 );
 var dLong = rad( lon2 - lon1 );
var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
 var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
 var d = R * c;
return d.toFixed(3); //Retorna tres decimales
 }



 var de = Math.round(getKilometros(doc.latitude,doc.longitude,Addres.latitude,Addres.longitude))
 var timpo = Math.round(de * 1.5)

  const {
    colors: {border},
  } = useTheme();
  return (
    <View style={styles.container}>
      <View style={styles.ratingContainer}>
       
      </View>
      <View style={styles.buttonContainer}>
        <Button
          style={[styles.button, {backgroundColor: border}]}
          icon={<Icon isPrimary name="map-marker-alt" size={10} />}>
          <Text isPrimary style={styles.buttonText}>{`${de} km`}</Text>
        </Button>
        <Button
          style={[styles.button, {backgroundColor: border}]}
          icon={<Icon isPrimary name="clock" size={10} />}>
          <Text isPrimary style={styles.buttonText}>{`${timpo} m`}</Text>
        </Button>
      </View>
    </View>
  );
};

export default PlaceCardInfo;
