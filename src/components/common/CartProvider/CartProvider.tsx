import * as React from 'react';
import CartContext, {CartItem,Addres} from '@src/context/cart-context';
import Api from '@src/Api';


type CartProviderProps = {};

const CartProvider: React.FC<CartProviderProps> = ({children}) => {
  const [cartItems, setCartItems] = React.useState<CartItem[]>([]);
  const [totalPrice, setTotalPrice] = React.useState(0);
  const [Addres, setAddres] = React.useState({longitude:0,latitude:0,address:"",longitudeDelta: 0.0022,
    latitudeDelta: 0.0031});
  const [driverNotes, set_driverNotes] = React.useState("");

var se = cartItems;
  
  const SetAddres = React.useCallback((long,lat) => {
      

      Api.GetAdressFromGeo(lat, long,(dir)=>{

        setAddres({latitude:lat, longitude:long,address:dir,longitudeDelta: 0.0022,
    latitudeDelta: 0.0031})

      });
     
      
    },
    [],
  );

  const updateCartItems = React.useCallback(
    (items: CartItem[], total: number) => {
      
      
      for (var i = 0; i < items.length; i++) {
        
        //se.push(items[i])
      }
      setCartItems(prevArray => [...prevArray, items[0]]);
      setTotalPrice(totalPrice+total);
      
    },
    [],
  );

  const setDriverNotes = React.useCallback(
    (text: string) => {
      
       set_driverNotes(text);
      
    },
    [],
  );

  const removeCartItem = React.useCallback((item: CartItem) => {
      
      var res = []
      for (var i = 0; i < cartItems.length; i++) {
        if(cartItems[i].dish.objectId!=item.dish.objectId){
          res.push(cartItems[i])   
        }
           
      } 

      
      setCartItems(res)
  },[cartItems]);
  const clearCart = React.useCallback((id:string) => {
    


    if(id!=undefined){
        var seq = []

        for (var i = 0; i < cartItems.length; ++i) {
          var item = cartItems[i]
          if(item.res!=id){
            seq.push(item)
          }
        }
        setCartItems(seq);
        setTotalPrice(0);
    }
    //setCartItems(cartItems);
    //
    //
    
  }, []);


  return (
    <CartContext.Provider
      value={{
        cartItems,
        updateCartItems,
        totalPrice,
        clearCart,
        removeCartItem,
        setDriverNotes,
        driverNotes,
        SetAddres,
        Addres
      }}>
      {children}
    </CartContext.Provider>
  );
};

export default CartProvider;
