import * as React from 'react';
import {Image, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Container, Text, Touchable} from '@src/components/elements';
import {Place} from '@src/data/mock-places';
import styles from './styles';
import PlaceCardInfo from '../PlaceCardInfo';

type PlaceListItemProps = {
  data: Place;
  doc:{}
};

const PlaceListItem: React.FC<PlaceListItemProps> = ({data,doc}) => {
  const {image, title, subTitle,id} = data;
  const navigation = useNavigation();

  const _onPlaceItemPressed = () => {
    navigation.navigate('PlaceDetailsScreen',{id:id,doc:doc});
  };

  return (
    <Touchable onPress={_onPlaceItemPressed}>
      <Container style={styles.container}>
        <Image style={styles.image} source={image} />
        <View style={styles.placeInfoContainer}>
          <View style={styles.placeInfo}>
            <Text style={styles.placeTitle}>{title}</Text>
            <Text style={styles.placeSubTitle}>{doc.descripcion}</Text>
          </View>
          <PlaceCardInfo doc={doc} data={data} />
        </View>
      </Container>
    </Touchable>
  );
};

export default PlaceListItem;
