import firebase from '@react-native-firebase/app';
import firestore from '@react-native-firebase/firestore';
import { AsyncStorage } from 'react-native';
import database from '@react-native-firebase/database';
import Geolocation from 'react-native-geolocation-service';
import Parse from "parse/react-native.js";
import CacheStore from 'react-native-cache-store';

class Api {



	static SetUserId(id) {
		CacheStore.set('objectId', id, 1000);
	}
	static GetUserId(res) {
		CacheStore.get('objectId').then((value) => {
			res(value)
		})
	}
	static DeleteUserId(res) {
		CacheStore.set('objectId', null, 1000);
	}
	static Pago = null
	static ShowEntrega = () => { }
	static OrderId = null;
	static OrderData = null;
	static User = null
	static UserData = {}
	static UserId = {}
	static UserRef = {}
	static Entrega = { value: '' }
	static Phone = null
	static OrdenSucursal = {}
	static DriverNotas = "";
	static param = null
	static GetCache = []
	static ToEngtrga = () => { }
	static GetAdressFromGeo(latitude, longitude, next) {


		var url = "https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyCfClc0mgzh5uC3FMcUPWh6RhFiEYAfNBY&latlng=" + latitude + "," + longitude;
		fetch(url).then(response => response.json()).then(res => {

			next(res.results[0].formatted_address)
		});
	}

	static GetGeo(next) {
		Geolocation.getCurrentPosition((position) => {
			const { longitude, latitude } = position.coords;
			next(longitude, latitude)


		},
			(error) => {
				console.log(error.code, error.message);
			},
			{ timeout: 15000, maximumAge: 10000 },
		);
	}
	static Conect() {


		const credentials = {
			apiKey: "AIzaSyAb7b-eYmrC3hL8kQI3g0UkMYjeFGKHBnA",
			authDomain: "delivery-23f4e.firebaseapp.com",
			databaseURL: "https://delivery-23f4e.firebaseio.com",
			projectId: "delivery-23f4e",
			storageBucket: "delivery-23f4e.appspot.com",
			messagingSenderId: "443526246860",
			appId: "1:443526246860:web:04106e0caf5b10b93e6d69"
		};

		const config = {
			name: 'SECONDARY_APP',
		};

		if (!firebase.apps.length) {
			firebase.initializeApp(credentials);
		}

	}
	static tmp = {}
	static tmp_er = {}
	static Save(document, data, next) {


		console.log(document)
		console.log(data)
		if ('objectId' in data) {

			var id = data.objectId
			delete data.objectId;
			delete data.createdAt;
			delete data.updatedAt;

			fetch('https://api.yammyfood.mx/parse/classes/' + document + "/" + id, {
				method: 'PUT',
				headers: {
					'X-Parse-Application-Id': 'SCWASRTWK9Y6AVMP3KFC_1',
					'X-Parse-REST-API-Key': "undefined",
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(data)
			}).then(res => res.json())
				.then((data) => {

					next(data.objectId)
				})

		} else {
			fetch('https://api.yammyfood.mx/parse/classes/' + document, {
				method: 'POST',
				headers: {
					'X-Parse-Application-Id': 'SCWASRTWK9Y6AVMP3KFC_1',
					'X-Parse-REST-API-Key': "undefined",
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(data)
			}).then(res => res.json())
				.then((data) => {
					console.log(data)
					next(data.objectId)
				})
		}

	}

	static List(document, next) {

		console.log(document)

		try {
			fetch('https://api.yammyfood.mx/parse/classes/' + document + '?where=' + JSON.stringify({}), {
				method: 'GET',
				headers: {
					'X-Parse-Application-Id': 'SCWASRTWK9Y6AVMP3KFC_1',
					'X-Parse-REST-API-Key': "undefined",
					'Content-Type': 'application/json'
				},
			}).then(res => res.json()).then((data) => {

				next(data.results)
			})

		} catch (error) {

			console.error(document);

			console.error(error);
		}

	}

	static ListOrder(document, sort, next) {
		console.log(document)
		console.log(sort)
		try {
			fetch('https://api.yammyfood.mx/parse/classes/' + document + '?order=' + sort, {
				method: 'GET',
				headers: {
					'X-Parse-Application-Id': 'SCWASRTWK9Y6AVMP3KFC_1',
					'X-Parse-REST-API-Key': "undefined",
					'Content-Type': 'application/json'
				},
			}).then(res => res.json())
				.then((data) => {

					next(data.results)
				})

		} catch (error) {

			console.error(document);
			console.error(sort);
			console.error(error);
		}

	}
	static Query(document, q, next) {

		var pa = "";

		if ('where' in q) {

			pa = pa + "where=" + JSON.stringify(q['where']);
		}

		if ('order' in q) {

			if (pa != "") {
				pa = pa + "&"
			}
			pa = pa + "order=" + q['order'];
		}

		try {
			fetch('https://api.yammyfood.mx/parse/classes/' + document + '?' + pa, {
				method: 'GET',
				headers: {
					'X-Parse-Application-Id': 'SCWASRTWK9Y6AVMP3KFC_1',
					'X-Parse-REST-API-Key': "undefined",
					'Content-Type': 'application/json'
				},
			}).then(res => res.json())
				.then((data) => {

					next(data.results)
				})

		} catch (error) {

			console.error(document);
			console.error(where);
			console.error(error);
		}
	}
	static Where(document, where, next) {
		console.log(document)
		console.log(where)
		try {
			fetch('https://api.yammyfood.mx/parse/classes/' + document + '?where=' + JSON.stringify(where), {
				method: 'GET',
				headers: {
					'X-Parse-Application-Id': 'SCWASRTWK9Y6AVMP3KFC_1',
					'X-Parse-REST-API-Key': "undefined",
					'Content-Type': 'application/json'
				},
			}).then(res => res.json())
				.then((data) => {

					next(data.results)
				})

		} catch (error) {

			console.error(document);
			console.error(where);
			console.error(error);
		}
	}

	static GetMulti(document, ids, next) {


		Api.Query(document, { where: { objectId: { "$in": ids } } }, (docs) => {
			next(docs)

		})

	}



	static GetDoc(document, id) {
		Api.Conect()


		const categoryDocRef = firebase.firestore()
			.collection(document)
			.doc(id);
		return categoryDocRef
	}


	static Get(document, id, next) {


		Api.Query(document, { where: { objectId: id } }, (data) => {
			next(data[0])
		})
	}

	static Parametros(codigo, next) {
		Api.Conect()




		var getValor = (data) => {

			for (var i = 0; i < data.length; i++) {
				if (data[i].codigo == codigo) {
					next(data[i].valor)
					return
				}
			}
		}
		if (Api.param != null) {


			getValor(Api.param)

		}

		Api.List('parametros', (data) => {
			Api.param = data
			getValor(Api.param)

		})


	}

	static GetUser(next) {


		Api.GetUserId((value) => {

			Api.Query('users', { where: { uid: value } }, (data) => {
				if (data.length == 0) {
					Api.Save('users', { uid: value }, (data) => {
						Api.GetUser(next);
					})
				} else {

					next(data[0], data[0].objectId)
				}
			})
		})
	}
	static OrderUser(next) {


		Api.GetUserId((value) => {

			Api.Query('ordenes', { where: { usuario: { "__type": "Pointer", "className": "users", "objectId": value }, 'status': { "$in": ['nuevo', 'para entrega', 'en_camino', 'preparando'] } } }, (data) => {
				next(data)
			})
		});

	}

	static Empresa(id, next) {
		const db = firebase.firestore();




		db.collection('empresas').doc(id).get().then(querySnapshot => {


			next(querySnapshot)

		})


	}
	static SaveUser(data, next) {
		Api.save('users', { ...data, objectId: Api.UserId }, () => {
			next()
		})

	}


	static ChatGet(next) {


		const onChildAdd = database().ref('/chat/' + Api.UserId).on('value', snapshot => {
			next(snapshot.val())
		})
	}


	static getKilometros(lat1, lon1, lat2, lon2) {
		var rad = (x) => { return x * Math.PI / 180; }
		var R = 6378.137; //Radio de la tierra en km
		var dLat = rad(lat2 - lat1);
		var dLong = rad(lon2 - lon1);
		var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c;
		return d.toFixed(3);
	}


	static ChatSend(sms) {





		database().ref('/chat/' + Api.UserId + '/' + Date.now()).set(sms).then(() => {

		});


	}

}


export default Api;