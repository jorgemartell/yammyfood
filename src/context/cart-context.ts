import * as React from 'react';
import {Dish} from '@src/data/mock-places';

export type CartItem = {
  dish: Dish;
  qty:0,
  priceunit:0;
  subtotal:0;
  res:"",
  sideDishes: Dish[];
  sub_p:[]
};

export type Radio = {
  value: string;
  label: string;
  rightElement?: React.ReactElement;
};

export type AddresItem = {
  longitude:number, 
  latitude:number,
  address:string,
  longitudeDelta: number,
  latitudeDelta: number,
};

export type CartState = {
  cartItems: CartItem[];
  updateCartItems: (items: CartItem[], totlaPrice: number) => void;
  SetAddres: (long: number,lat: number) => void;
  Addres:AddresItem,
  removeCartItem: (item: CartItem) => void;
  addPayment: (item:Radio) => void;
  driverNotes:string;
  setDriverNotes:(text:string) => void;
  totalPrice: number;
  clearCart: (id:string) => void;
};

const initialCartState: CartState = {
  cartItems: [],
  updateCartItems: () => {},
  removeCartItem: () => {},
  Addres:{longitude:0, latitude:0,address:""},
  SetAddres: () => {},
  addPayment: () => {},
  driverNotes:"",
  setDriverNotes:() => {},
  totalPrice: 0,
  clearCart: (id:string) => {},
};

export default React.createContext(initialCartState);
