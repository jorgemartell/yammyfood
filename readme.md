


## Installation
```
git clone https://gitlab.com/jorgemartell/yammyfood.git
cd yammyfood
yarn install/npm install
cd ios & pod install
```

## Running

```
cd yammyfood
yarn android
yarn ios
```


## Build android apk & aab

```
cd yammyfood
yarn build:apk
yarn build:aab
```
