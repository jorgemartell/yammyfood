/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import messaging from '@react-native-firebase/messaging';

messaging().setBackgroundMessageHandler(async remoteMessage => {
  console.log('Message handled in the background!', remoteMessage);
});


async function requestUserPermission() {
  const authorizationStatus = await messaging().requestPermission();

  if (authorizationStatus) {
    console.log('Permission status:', authorizationStatus);
  }
}


//messaging().getIsHeadless().then(isHeadless => {
  // do sth with isHeadless
//});

var  HeadlessCheck = ({ isHeadless })=> {
  if (isHeadless) {
    // App has been launched in the background by iOS, ignore
    console.log('Message handled in the background!')
    return null;
  }

  requestUserPermission()
  return App();
}


AppRegistry.registerComponent(appName, () =>HeadlessCheck);
